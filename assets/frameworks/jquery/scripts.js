var api = "http://billing.gooptix.id/";
//var api = "http://localhost/biling/";
jQuery(document).ready(function($) {
	
	//ExportTable();
	$('tr').click(function(event) {
			/* Act on the event */
			//console.log(this);
			$('.selected').removeClass('selected');
			$(this).addClass('selected');
			//console.log($(".selected td:nth-child(8)").text());
			$('#no_inv').val($(".selected td:nth-child(2)").text());
			$('#nama').val($(".selected td:nth-child(4)").text());
			$('#tagihan').val($(".selected td:nth-child(6)").text());
			$('#restitusi').val($(".selected td:nth-child(7)").text());
			$('#diskon').val($(".selected td:nth-child(8)").text());
			$('#no_account').val($(".selected td:nth-child(11)").text());
			$('#bulan').val($(".selected td:nth-child(5)").text());
			//alert($(".selected td:nth-child(6)").text());
			var id = $(".selected td:nth-child(2)").text();
			var  urut = id.substr(id.length - 5); // => "Tabs1"
			var inturut = parseInt(urut);
			//console.log(inturut);
			//console.log($(".selected td:nth-child(9)").text());
			$('#custid').val($(".selected td:nth-child(9)").text());
			

			$.ajax({
				url: api+'api/customer/regid',
				type: 'get',
				dataType : 'json',
				data: {urut : inturut},
				contentType: 'application/json',
				success: function (data) {
					//alert("berhasil2");
					//data
					//console.log(data);
					//$('#custid').val(data[0].Reg_ID);
				},

			});
		});

	var arrayinv = [];   
	$('input[type="checkbox"]').change(function() {
	     if(this.checked) {
	     	 var noinv = "'"+$(this).val()+"'";
	     	 console.log(noinv);
	         arrayinv.push(noinv);
	    	 
	    	 $('#arrayinvoice').val(arrayinv);
	    	 console.log(arrayinv);
	     }
	     else{
	     	function arrayRemove(arr, value) {

			   return arr.filter(function(ele){
			       return ele != value;
			   });

			}
	     	//console.log($(this).val());
	     	var noinv = "'"+$(this).val()+"'";
	     	var arraynew = arrayRemove(arrayinv, noinv);
	     	arrayinv = arraynew;
	     	$('#arrayinvoice').val(arrayinv);
	     	console.log(arrayinv);
	     }
	 });

	var arraycb = [];   
	$('input[type="checkbox"]').change(function() {
	     if(this.checked) {
	     	 //var noinv = "'"+$(this).val()+"'";
	     	 //console.log(noinv);
	         arraycb.push($(this).val());
	    	 
	    	 $('#arraycb').val(arraycb);
	    	 console.log(arraycb);
	     }
	     else{
	     	function arrayRemove(arr, value) {

			   return arr.filter(function(ele){
			       return ele != value;
			   });

			}
	     	//console.log($(this).val());
	     	//var noinv = "'"+$(this).val()+"'";
	     	var arraynew = arrayRemove(arraycb, $(this).val());
	     	arraycb = arraynew;
	     	$('#arraycb').val(arraycb);
	     	console.log(arraycb);
	     }
	 });

	//alert($('#loginname').val());
	$.removeCookie('identity');
	$.cookie('identity', $('#loginname').val());

	$('#submitstatusinv').click(function () {
		//alert($('#bulan').val());
		var mystr = $('#bulan').val();
		var datearr = mystr.split("-");
		var month = datearr[1];
		var year = datearr[0];
	    window.location.href = api+'admin/invoice/update_invoice?inv_no='+$('#no_inv').val()+'&statusinv='+$('#statusinv').val()+'&bulan='+month+'&tahun='+year+'&reg_id='+$('#custid').val();
	    return false;
    
	});
	$('#submitstatusinvretail').click(function () {
		alert($('#bulan').val());
		var mystr = $('#bulan').val();
		var datearr = mystr.split("-");
		var month = parseInt(datearr[1]);
		var year = datearr[0];
		alert(api+'admin/invoice/update_invoice_retail?inv_no='+$('#no_inv').val()+'&statusinv='+$('#statusinv').val()+'&bulan='+month+'&tahun='+year+'&reg_id='+$('#custid').val());
		//console.log();
	    window.location.href = api+'admin/invoice/update_invoice_retail?inv_no='+$('#no_inv').val()+'&statusinv='+$('#statusinv').val()+'&bulan='+month+'&tahun='+year+'&reg_id='+$('#custid').val();
	    return false;
    
	});


	/*
		Invoice Summary
	*/
	getInvlist();
	//document.cookie = "identity=".$('#loginname').val();
	$.ajax({
			url: api+'api/invoice/name',
			type: 'get',
			dataType : 'json',
			data: {},
			contentType: 'application/json',
			success: function (data) {
				//alert("berhasil2");
				//data
				//console.log(data);
				var sHtml = '';
				sHtml +='<option value="">Pilih Nama Akun</option>';
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<option value="'+val.Account_Name+'">'+val.Account_Name+'</option>';

				});
				$('#nama').html(sHtml);
			},

		});

	$.ajax({
			url: api+'api/invoice/invdate',
			type: 'get',
			dataType : 'json',
			data: {},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				sHtml +='<option value="">Pilih Bulan Invoice</option>';
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<option value="'+val.Inv_Date+'">'+val.Inv_Date+'</option>';

				});
				$('#date').html(sHtml);
			},

		});

	$.ajax({
			url: api+'api/invoice/subproduct',
			type: 'get',
			dataType : 'json',
			data: {},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				sHtml +='<option value="">Pilih Produk</option>';
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<option value="'+val.Sub_Product+'">'+val.Sub_Product+'</option>';

				});
				$('#produk').html(sHtml);
			},

		});

	$.ajax({
			url: api+'api/invoice/city',
			type: 'get',
			dataType : 'json',
			data: {},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				sHtml +='<option value="">Pilih kota</option>';
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<option value="'+val.City+'">'+val.City+'</option>';

				});
				$('#city').html(sHtml);
			},

		});

	$('#filterinvoice').click(function(event) {
		/* Act on the event */
		var city = $('#city').val();
		var invdate = $('#date').val();
		var name = $('#nama').val();

		window.location.href = api+'admin/invoice?city='+city+'&name='+name+'&invdate='+invdate;
		//http://billing.gooptix.id/
	});

	$('#filterinvoiceretail').click(function(event) {
		/* Act on the event */
		var city = $('#city').val();
		var invdate = $('#date').val();
		var name = $('#nama').val();

		window.location.href = api+'admin/invoice/retail?city='+city+'&name='+name+'&invdate='+invdate;
		//http://billing.gooptix.id/
	});


	function getSubname($subnama){
		$.ajax({
			url: api+'api/invoice/subname/'+$subnama,
			type: 'get',
			dataType : 'json',
			data: {},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				sHtml +='<option value="">Pilih Sub Nama</option>';
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<option value="'+val.Account_Sub_Name+'">'+val.Account_Sub_Name+'</option>';

				});
				$('#subnama').html(sHtml);
			},

		});

	}

	function getInvlist($name,$subname,$invdate,$product){

		$.ajax({
			url: api+'api/invoice/where',
			type: 'get',
			dataType : 'json',
			data: {name : $name,subname : $subname,inv_date : $invdate,sub_product : $product},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<tr>';
					 sHtml +='<td>'+val.Inv_No+'</td>';
					 sHtml +='<td>'+val.Account_Name+'</td>';
					 sHtml +='<td>'+val.Account_Sub_Name+'</td>';
					 sHtml +='<td>'+val.Inv_Date+'</td>';
					 sHtml +='<td>'+val.Subtotal+'</td>';
					 sHtml +='<td>'+val.Other_Val+'</td>';
					 sHtml +='<td>'+val.Discount_Val+'</td>';
					 sHtml +='<td>'+val.Total+'</td>';
					 sHtml +='<td>'+val.Sub_Product+'</td>';
					 sHtml +='<td>Detail</td>';
					 sHtml +='</tr>';

				});
				$('#invlist').html('');
				$('#invlist').html(sHtml);

			},


		});

	}

	$('#nama').change(function(event) {
		/* Act on the event */
		getSubname($('#nama :selected').val());
		getInvlist($('#nama :selected').val(),$('#subnama :selected').val(),$('#date :selected').val(),$('#produk :selected').val());
	});

	$('#subnama').change(function(event) {
		/* Act on the event */
		getInvlist($('#nama :selected').val(),$('#subnama :selected').val(),$('#date :selected').val(),$('#produk :selected').val());
	});

	$('#date').change(function(event) {
		/* Act on the event */
		getInvlist($('#nama :selected').val(),$('#subnama :selected').val(),$('#date :selected').val(),$('#produk :selected').val());
	});

	$('#produk').change(function(event) {
		/* Act on the event */
		getInvlist($('#nama :selected').val(),$('#subnama :selected').val(),$('#date :selected').val(),$('#produk :selected').val());
	});

	/*
		proforma Invoice Log
	*/

	$.ajax({
			url: api+'api/invoice/proforma_log',
			type: 'get',
			dataType : 'json',
			//data: {invno : $invno},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<tr>';
					 sHtml +='<td>'+val.Inv_No+'</td>';
					 sHtml +='<td>'+val.Created_By+'</td>';
					 sHtml +='<td>'+val.Dateinsert+'</td>';
					 sHtml +='<td>'+val.Status+'</td>';
					 sHtml +='</tr>';

				});
				$('#profloglist').html('');
				$('#profloglist').html(sHtml);
			},

		});
	//getProformalog();

	function getProformalogfilter($invno){
		$.ajax({
			url: api+'api/invoice/proforma_log',
			type: 'get',
			dataType : 'json',
			data: {invno : $invno},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<tr>';
					 sHtml +='<td>'+val.Inv_No+'</td>';
					 sHtml +='<td>'+val.Created_By+'</td>';
					 sHtml +='<td>'+val.Dateinsert+'</td>';
					 sHtml +='<td>'+val.Status+'</td>';
					 sHtml +='</tr>';

				});
				$('#profloglist').html('').fadeOut('slow');
				$('#profloglist').html(sHtml).fadeIn('slow');
			},

		});
	}
	$("#keyinv").on("keyup", function() {
	   getProformalogfilter($('#keyinv').val());
	   //console.log($('#keyinv').val());
	});
	
	/* invoice Log */

	$.ajax({
			url: api+'api/invoice/invoice_log',
			type: 'get',
			dataType : 'json',
			//data: {invno : $invno},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<tr>';
					 sHtml +='<td>'+val.Inv_No+'</td>';
					 sHtml +='<td>'+val.Created_By+'</td>';
					 sHtml +='<td>'+val.Dateinsert+'</td>';
					 sHtml +='<td>'+val.Status+'</td>';
					 sHtml +='</tr>';

				});
				$('#invloglist').html('');
				$('#invloglist').html(sHtml);
			},

		});
	//getProformalog();

	function getInvoicelogfilter($invno){
		$.ajax({
			url: api+'api/invoice/invoice_log',
			type: 'get',
			dataType : 'json',
			data: {invno : $invno},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 sHtml +='<tr>';
					 sHtml +='<td>'+val.Inv_No+'</td>';
					 sHtml +='<td>'+val.Created_By+'</td>';
					 sHtml +='<td>'+val.Dateinsert+'</td>';
					 sHtml +='<td>'+val.Status+'</td>';
					 sHtml +='</tr>';

				});
				$('#invloglist').html('').fadeOut('slow');
				$('#invloglist').html(sHtml).fadeIn('slow');
			},

		});
	}
	$("#keyinv1").on("keyup", function() {
	   getInvoicelogfilter($('#keyinv1').val());
	   //console.log($('#keyinv1').val());
	});

	// Dashboard 
	var year = new Date().getFullYear();      // Get the four digit year (yyyy)
	var month = new Date().getMonth()+1;
	var month2 = new Date().getMonth();
	var bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
	var namabulan = bulan[month2];

	//alert(namabulan);        // Get the month (0-11)
	getInvoiceCreated(month,year);
	getInvoiceClosed(month,year);
	getValueCreated(month,year);
	getValueClosed(month,year);

	$('#namabulan').html('<h3>Pendapatan Per Bulan '+namabulan+'</h3>');

	$('#date').change(function(event) {
		/* Act on the event */
		//alert('tes');
		var newdate = $('#date option:selected').val();
		var res = newdate.split("-");
		var month = res[1];
		var year = res[0];
		getInvoiceCreated(month,year);
		getInvoiceClosed(month,year);
		getValueCreated(month,year);
		getValueClosed(month,year);
		$('#namabulan').html('<h3>Pendapatan Per Bulan '+namabulan+'</h3>');
	});




	function getInvoiceCreated($bulan,$tahun){
		$.ajax({
			url: api+'api/invoice/invoice_month',
			type: 'get',
			dataType : 'json',
			data: {bulan : $bulan , tahun : $tahun},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 //alert(val.jumlah_invoice);
					 sHtml +='<h4>'+val.jumlah_invoice+'</h4><p>Total Invoice</p>';

				});
				$('#totalinv').html(sHtml);

			},

		});
	}

	function getInvoiceClosed($bulan,$tahun){
		$.ajax({
			url: api+'api/invoice/invoice_closed_month',
			type: 'get',
			dataType : 'json',
			data: {bulan : $bulan , tahun : $tahun},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 //alert(val.jumlah_invoice);
					 sHtml +='<h4>'+val.jumlah_invoice+'</h4><p>Invoice Closed</p>';

				});
				$('#closedinv').html(sHtml);

			},

		});
	}

	function getValueCreated($bulan,$tahun){
		$.ajax({
			url: api+'api/invoice/value_month',
			type: 'get',
			dataType : 'json',
			data: {bulan : $bulan , tahun : $tahun},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 //alert(val.jumlah_invoice);
					 var nilai = parseInt(val.nilai_invoice);
					 
					 sHtml +='<h4>Rp. '+nilai.toLocaleString()+'</h4><p>Perkiraan Pendapatan<input type="hidden" id="perkiraan" value="'+nilai+'"></p>';

				});
				$('#valueinv').html(sHtml);

			}

		});
	}

	function getValueClosed($bulan,$tahun){
		$.ajax({
			url: api+'api/invoice/value_closed_month',
			type: 'get',
			dataType : 'json',
			data: {bulan : $bulan , tahun : $tahun},
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				var sHtml = '';
				var xHtml = '';
				var sisanya = '';
				var perkiraan = $('#perkiraan').val();
				
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val);
					 //alert(val.jumlah_invoice);
					 var nilaiinvoice = parseInt(val.nilai_invoice);
					 sHtml +='<h4>Rp. '+nilaiinvoice.toLocaleString()+'</h4><p>Pembayaran Diterima</p>';
					 var sisanya = perkiraan - val.nilai_invoice;
					 xHtml = '<h4>Rp. '+sisanya.toLocaleString()+'</h4><p>Outstanding</p>';

				});

				
				
				$('#valueclosed').html(sHtml);
				$('#valueoutstanding').html(xHtml);

			},

		});
	}
	var invdate=[];
	var created=[];
	var closed=[];
	$.ajax({
			url: api+'api/invoice/value',
			type: 'get',
			dataType : 'json',
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val.Inv_Date);
					 invdate.push(val.inv_date);
					 created.push(val.nilai_invoice);
				});
				//getChart();

			}
		});
	$.ajax({
			url: api+'api/invoice/value_closed',
			type: 'get',
			dataType : 'json',
			contentType: 'application/json',
			success: function (data) {
				//console.log(data);
				$.each(data, function(index, val) {
					 /* iterate through array or object */
					 //console.log(val.Inv_Date);
					 //invdate.push(val.inv_date);
					 closed.push(val.nilai_invoice);
				});
				getChart();

			}
		});
	//console.log(created);

	var barChartData = {

			labels: invdate,
			datasets: [{
				label: 'Invoice Out',
				backgroundColor: window.chartColors.red,
				stack: 'Stack 0',
				data: created
			},
			{
				label: 'Invoice Closed',
				backgroundColor: window.chartColors.blue,
				stack: 'Stack 1',
				data: closed
			}]
		};
		function getChart(){
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Resume Invoice'
					},
					tooltips: {
						mode: 'index',
						intersect: false,
						callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                     	function numberWithCommas(x) {
				        var parts = x.toString().split(".");
				        parts[0]=parts[0].replace(/\B(?=(\d{3})+(?!\d))/g,".");
				        return parts.join(",");
				        }
                    label += 'Rp.'+numberWithCommas(tooltipItem.yLabel);
                    return label;
                }
            }
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					}
				}
			});
		};


		jQuery(document).ready(function($) {
			
			//console.log('tes');
		});
		var namapelanggan = [];
		var provinsi = [];
		var kota = [];
		$.ajax({
				url: api+'api/customer/name',
				type: 'get',
				dataType : 'json',
				success: function (data) {
					//console.log(data);
					
					$.each(data, function(index, val) {
						 /* iterate through array or object */
						 namapelanggan.push(val.Customer_Name);
					});
					//console.log(namapelanggan)

				}
			});

		$.ajax({
				url: api+'api/daerah/provinsi',
				type: 'get',
				dataType: 'json',
				success: function (data) {
					$.each(data, function(index, val) {
						 /* iterate through array or object */
						 provinsi.push(val.nama)
					});
				}
			});

		$.ajax({
				url: api+'api/daerah/kota',
				type: 'get',
				dataType: 'json',
				success: function (data) {
					$.each(data, function(index, val) {
						 /* iterate through array or object */
						 kota.push(val.nama)
					});
				}
			});
		$(function() {
		    $("#namapelanggan").autocomplete({
		      source: namapelanggan
		    });
		    $("#provinsi").autocomplete({
		      source: provinsi
		    });
		    $("#kota").autocomplete({
		      source: kota
		    });
		  } );
		$('#filtercustomer').click(function(event) {
			/* Act on the event */
			//alert('tes');
			window.location.href = api+'admin/customer?city='+$('#city').val()+'&status='+$('#status').val();
			return false;
		});


		function ExportTable(){
			$("table").tableExport({
				headings: true,                    // (Boolean), display table headings (th/td elements) in the <thead>
				footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
				formats: ["xls", "csv", "txt"],    // (String[]), filetypes for the export
				fileName: "id",                    // (id, String), filename for the downloaded file
				bootstrap: true,                   // (Boolean), style buttons using bootstrap
				position: "well" ,                // (top, bottom), position of the caption element relative to table
				ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file
				ignoreCols: null,                 // (Number, Number[]), column indices to exclude from the exported file
				ignoreCSS: ".tableexport-ignore"   // (selector, selector[]), selector(s) to exclude from the exported file
			});
		}


		function get_list_cashback(){

			$.ajax({
				url: api+'api/daerah/kota',
				type: 'get',
				dataType: 'json',
				success: function (data) {
					$.each(data, function(index, val) {
						 /* iterate through array or object */
						 kota.push(val.nama)
					});
				}
			});

		};

		
});
function get_list_mitra() {
	// body...
	$.ajax({
		url: api+'api/cashback/mitralist',
		type: 'get',
		dataType: 'json',
		success: function (data) {
			console.log(data);
			var sHtml = '';
			sHtml +='<option value="">Pilih Mitra</option>';
			$.each(data, function(index, val) {
				 /* iterate through array or object */
				 //kota.push(val.nama)
				 sHtml +='<option value="'+val.id+'">'+val.name+'</option>';

			});
			$('#namamitra').html(sHtml);
		}
	});
}

function get_list_customer(){
	$.ajax({
		url: api+'api/customer/name',
		type: 'get',
		dataType : 'json',
		data: {},
		contentType: 'application/json',
		success: function (data) {
			//alert("berhasil2");
			//data
			//console.log(data);
			var sHtml = '';
			sHtml +='<option value="">Pilih Nama Pelanggan</option>';
			$.each(data, function(index, val) {
				 /* iterate through array or object */
				 //console.log(val);
				 sHtml +='<option value="'+val.Task_ID+'">'+val.Customer_Name+'</option>';

			});
			$('#namacust').html(sHtml);
		},

	});
}