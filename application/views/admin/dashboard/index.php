<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                   
                   <div class="form-group">
                    <label id="namabulan" for="sel1"> </label>
                    <select id="date" class="form-control">
                    </select>
                  </div>
                    

                    <div class="row">
                        <div class="col-lg-2 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-aqua">
                            <div id="totalinv" class="inner">
                              
                            </div>
                            <div class="icon">
                              <i class="fa  fa-area-chart"></i>
                            </div>
                            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                          </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-yellow">
                            <div id="closedinv" class="inner">
                              <h3>53<sup style="font-size: 20px">%</sup></h3>

                              <p>Proses Invoice</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-print"></i>
                            </div>
                            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                          </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-green">
                            <div id="valueinv" class="inner">
                              <h3>44</h3>

                              <p>Perkiraan Pendapatan</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-money"></i>
                            </div>
                            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                          </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-red">
                            <div id="valueclosed" class="inner">
                              <h3>65</h3>

                              <p>Pembayaran Diterima</p>
                            </div>
                            <div class="icon">
                              <i class="ion ion-pie-graph"></i>
                            </div>
                            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                          </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                          <!-- small box -->
                          <div class="small-box bg-blue">
                            <div id="valueoutstanding" class="inner">
                              <h3>65</h3>

                              <p>Invoice on Progress</p>
                            </div>
                            <div class="icon">
                              <i class=" fa fa-file-text"></i>
                            </div>
                            <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                          </div>
                        </div>
                      </div>
                    <!-- ROW 2-->
                    <!-- <h3>HISTORY CLOSED TICKET</h3>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green">D</span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Today</span>
                                    <span class="info-box-number"><?php 
                                    $total = count((array)$t_today);
                                    echo $total; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-orange">W</span>
                                <div class="info-box-content">
                                    <span class="info-box-text">This Week</span>
                                    <span class="info-box-number"><?php 
                                    $total = count((array)$t_week);
                                    echo $total; ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow">M</span>
                                <div class="info-box-content">
                                    <span class="info-box-text">This Month</span>
                                    <span class="info-box-number"><?php 
                                    $total = count((array)$t_month);
                                    echo $total; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-blue">Y</span>
                                <div class="info-box-content">
                                    <span class="info-box-text">This Year</span>
                                    <span class="info-box-number"><?php 
                                    $total = count((array)$t_year);
                                    echo $total; ?></span>
                                </div>
                            </div>
                        </div>
                    </div> -->

                      <!-- solid sales graph -->
          <div class="box box-solid bg-teal-gradient">
            <div class="box-header">
              <i class="fa fa-th"></i>

              <h3 class="box-title">Sales Graph</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <div class="box-body border-radius-none">
              <div style="width: 100%">
                <canvas id="canvas"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
                    
                </section>
            </div>
