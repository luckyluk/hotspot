<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1> Customer </h1>
                    <?php echo $breadcrumb; ?>
                </section>
                <div class="col-md-12">
                      
                      <!--<div class="form-group col-md-3">
                        <select class="form-control" id="bulan" style="width:100%">
                          <option value="">Bulan Belum Bayar Bayar </option>
                          <option>9</option>
                          <option>10</option>
                          <option>11</option>
                          <option>12</option>
                        </select>
                          
                        </select>
                      </div>-->
                      <div class=" form-group col-md-3">
                        <select class="form-control" id="city" style="width:100%">
                          <option>Kota </option>
                          
                        </select>
                      </div>
                      <div class=" form-group col-md-3">
                        <select class="form-control" id="status" style="width:100%">
                          <option value="">Status </option>
                          <option value="2">Mati </option>
                          
                        </select>
                      </div>
                      <button id="filtercustomer" class="btn btn-primary col-md-1"><i class="fa fa-search"></i></button>
                    </div>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">                               
                                 <div class="box-body">
                                  <table class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th scope="col">NO</th>
                                        <th scope="col">Registration ID</th>
                                        <th scope="col">Kota</th>
                                        <th scope="col">Nama Akun</th>
                                        <th scope="col">Nama Sub Akun</th>
                                        <th scope="col">Status Link</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>

                                      <?php $index = 1; foreach ($customer as $key) {
                                          # code...

                                        if($key->status == 2){
                                          $status = "Mati";
                                          $btnstatus = "<a id='modal-501528' href='#' role='button' class='btn btn-danger' data-toggle='modal'>Mati</a>";
                                          $btnedit = "<a id='modal-501528' href='customer/onlink/".$key->Reg_ID."' role='button' class='btn btn-app btn-success' data-toggle='modal'><i class='fa fa-toggle-on'></i>Hidupkan</a>";

                                        }
                                        else{
                                          $status = "Hidup";
                                          $btnstatus = "<a id='modal-501528' href='#' role='button' class='btn btn-success' data-toggle='modal'>Hidup</a>";
                                          $btnedit = "<a id='modal-501528' href='customer/offlink/".$key->Reg_ID."' role='button' class='btn btn-app btn-danger' data-toggle='modal'><i class = ' fa fa-toggle-off'></i>Matikan</a>";
                                        }
                                          echo "<tr>";
                                          echo "<td>".$index++."</td>";
                                          echo "<td>".$key->Reg_ID."</td>";
                                          echo "<td>".$key->City."</td>";
                                          echo "<td>".$key->Customer_Name."</td>";
                                          echo "<td>".$key->Customer_Sub_Name."</td>";
                                          echo "<td>".$btnstatus."</td>";
                                          echo "<td>".$btnedit."<a target = '_blank' href='customer/detail/".$key->Reg_ID."' role='button' class='btn btn-app btn-danger' data-toggle='modal'><i class = 'fa fa-info-circle'></i>Detail</a><a target = '_blank' href='invoice/user/".$key->Reg_ID."' role='button' class='btn btn-app btn-danger' data-toggle='modal'><i class = 'fa fa-print'></i>Print Invoice</a></td>";
                                          echo "</tr>";
                                      }
                                      ?> 
                                      
                                    </tbody>
                                  </table>
                              </div>
                            </div>
                          </div>
                         </div>
                         </section> 

                <div class="modal fade" id="modal-container-501528" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form >
                          <div class="modal-header">
                              <h5 class="modal-title" id="myModalLabel">
                                  Detail User
                              </h5> 
                              <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">×</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              
                                  <div class="form-group">
                                       <input type="hidden" id="custid" value="">
                                      <label for="exampleInputEmail1">
                                          No. Invoice
                                      </label>
                                      <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Update Status
                                      </label><br>
                                      <select name="statusinv" id="statusinv">
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="CLOSED">CLOSED</option>
                                      </select>
                                  </div>
                                  
                            </div>
                          <div class="modal-footer">
                               
                              <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                      Submit
                              </button>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                  Close
                              </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
            
            </div>
