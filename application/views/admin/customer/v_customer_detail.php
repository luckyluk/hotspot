<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
            <div class="content-wrapper" style="background-color: #fff;">
                <section class="content-header">
                    <h1>Customer Detail </h1>
                    <?php echo $breadcrumb; ?>
                </section>
                <section class="content">
                	<div class="col-md-12  connectedSortable ui-sortable ">
                		<div class="col-md-6 ">
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >No. Registrasi</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Reg_ID ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Nama Customer</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Customer_Name ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Nama Sub Customer</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Customer_Sub_Name ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Alamat Customer</label>
                              <div class="col-sm-8">          
                                <textarea class="form-control" rows="5" id="comment" disabled><?php echo $customer[0]->Customer_Sub_Address ?></textarea>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >PIC</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->PIC ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Telephone</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Phone ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Handphone</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Handphone ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Mitra</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->name ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Cashback</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->cashback ?>" disabled>
                              </div>
                            </div><br>
                        </div>


                			<!--<label class="control-label col-sm-2">No. Registrasi</label><p>
                            <label class="control-label col-sm-2">Nama Customer</label>
                            <p>Nama  sub Customer</p><br>
                            <p>Alamat  Customer</p><br>
                			<p>Pic </p><br>
                			<p>Handpone</p><br>
                            <p>No Telepon </p><br></b>
                			
                			
                			
                		</div>
                		<div class="col-md-4">
                            <?php 
                			echo '<input class="form-control" id="disabledInput" type="text" disabled value="'.$customer[0]->Reg_ID.'"></p><br>';
                            echo '<input class="form-control" id="disabledInput" type="text" disabled value="'.$customer[0]->Customer_Name.'"></p><br>';
                            echo '<input class="form-control" id="disabledInput" type="text" disabled value="'.$customer[0]->Customer_Sub_Name.'"></p><br>';
                            echo '<input class="form-control" id="disabledInput" type="text" disabled value="'.$customer[0]->Customer_Sub_Address.'"></p><br>';
                            echo '<input class="form-control" id="disabledInput" type="text" disabled value="'.$customer[0]->PIC.'"></p><br>';
                            echo '<input class="form-control" id="disabledInput" type="text" disabled value="'.$customer[0]->Handphone.'"></p><br>';
                            echo '<input class="form-control" id="disabledInput" type="text" disabled value="'.$customer[0]->Phone.'"></p><br>';
                			
                            ?>
                		</div>
                		<div class="col-md-3">
                			
                			
                			<b><p>Email </p><br>
                			<p>Status</p><br>
                			<p>Note  </p><br>
                            <p>Tanggal Aktivasi </p><br>
                            <p>Start Biling</p><br>
                            <p>Biaya Bulanan </p><br>
                			<p>Biaya Installasi  </p><br></b>
                		</div>
                		<div class="col-md-3">
                			<p> : </p><br>
                			<p> : </p><br>
                			<p> : </p><br>
                			<p> : </p><br>
                			<p> : </p><br>
                			<p> : </p><br>
                			<p> : </p><br>
                		</div>-->
                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Email</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Email ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Status</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Status ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Note</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Note ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Tanggal Aktivasi</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Activation_Date ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Start Billing</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Start_Billing ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Biaya Instalasi</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Installation ?>" disabled>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Biaya Bulanan</label>
                              <div class="col-sm-8">          
                                <input type="text" class="form-control" value="<?php echo $customer[0]->Monthly_Price ?>" disabled>
                              </div>
                              </div><br>

                              <?php

                                function tgl_indo_1($tanggal){
                                  $bulan = array (
                                    1 =>   'Januari',
                                    'Februari',
                                    'Maret',
                                    'April',
                                    'Mei',
                                    'Juni',
                                    'Juli',
                                    'Agustus',
                                    'September',
                                    'Oktober',
                                    'November',
                                    'Desember'
                                    
                                  );
                                  $pecahkan = explode('-', $tanggal);
                                  
                                  // variabel pecahkan 0 = tanggal
                                  // variabel pecahkan 1 = bulan
                                  // variabel pecahkan 2 = tahun
                                 
                                  return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                                }

                                $taskid = $customer[0]->Task_ID;
                                $year = date("y");
                                $month= date("m");
                                $number = sprintf("%08d", $taskid);
                                $inv = "INS".$year."".$month."".$number;
                                $prf = "PRF-".$inv;
                                $year4 = date("Y");
                                $periode = $year4."-".$month."-01";
                                $pecahkan = explode('-', $customer[0]->Start_Billing);

                               ?>

                              <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Invoice Instalasi</label>
                              <div class="col-sm-8">          
                                <a class="btn btn-primary" href="<?php echo base_url('admin/invoice/create_proforma_instalasi/'.$customer[0]->Reg_ID)?>" role="button"> Buat Proforma Invoice Instalasi </a>
                              </div>
                            </div><br>
                            <div class="form-group col-md-12">
                              <label class="control-label col-sm-4" for="pwd" >Invoice Prorate</label>
                              <div class="col-sm-8">
                              <form action="<?php echo base_url('admin/invoice/create_proforma_prorate')?>" method="post"> 
                                <input type="hidden" name="regid" value="<?php echo $customer[0]->Reg_ID ?>">         
                                <input type="text" name="awal" value="<?php echo $pecahkan[2] ?>" style="width: 30px;"> s/d <input type="text" name="akhir" value="<?php echo date('t', strtotime($customer[0]->Start_Billing));?>" style="width: 30px;"> <?php echo tgl_indo_1($customer[0]->Start_Billing); ?>
                                <br> <input class="btn btn-primary" type="submit" value="Buat Proforma Prorate">
                            </form>
                              </div>
                            </div><br>
                        

                    </div>
                    <!--<div class=" row col-md-12">  
                        <?php 
                        if ($outstanding_2018[0]->jan == 1) {
                            $jan = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $jan = "<font color='red'>-";
                        }
                        if ($outstanding_2018[0]->feb == 1) {
                            $feb = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $feb = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->mar == 1) {
                            $mar = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $mar = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->apr == 1) {
                            $apr = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $apr = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->mei == 1) {
                            $mei = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $mei = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->jun == 1) {
                            $jun = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $jun = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->jul == 1) {
                            $jul = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $jul = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->agu == 1) {
                            $agu = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $agu = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->sep == 1) {
                            $sep = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $sep = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->okt == 1) {
                            $okt = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $okt = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->nov == 1) {
                            $nov = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $nov = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding_2018[0]->des == 1) {
                            $des = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $des = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }


                        ?>
                        2018
                        <table class="table table-bordered" style="background-color: white"> 
                            <thead>
                              <tr>
                                <th class="text-center">Januari</th>
                                <th class="text-center">Februari</th>
                                <th class="text-center">Maret</th>
                                <th class="text-center">April</th>
                                <th class="text-center">Mei</th>
                                <th class="text-center">Juni</th>
                                <th class="text-center">Juli</th>
                                <th class="text-center">Agustus</th>
                                <th class="text-center">September</th>
                                <th class="text-center">Oktober</th>
                                <th class="text-center">November</th>
                                <th class="text-center">Desember</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="text-center"><?php echo $jan;?></td>
                                <td class="text-center"><?php echo $feb;?></td>
                                <td class="text-center"><?php echo $mar;?></td>
                                <td class="text-center"><?php echo $apr;?></td>
                                <td class="text-center"><?php echo $mei;?></td>
                                <td class="text-center"><?php echo $jun;?></td>
                                <td class="text-center"><?php echo $jul;?></td>
                                <td class="text-center"><?php echo $agu;?></td>
                                <td class="text-center"><?php echo $sep;?></td>
                                <td class="text-center"><?php echo $okt;?></td>
                                <td class="text-center"><?php echo $nov;?></td>
                                <td class="text-center"><?php echo $des;?></td>
                              </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class=" row col-md-12">
                        <?php 
                        if ($outstanding[0]->jan == 1) {
                            $jan = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $jan = "<font color='red'>-";
                        }
                        if ($outstanding[0]->feb == 1) {
                            $feb = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $feb = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->mar == 1) {
                            $mar = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $mar = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->apr == 1) {
                            $apr = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $apr = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->mei == 1) {
                            $mei = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $mei = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->jun == 1) {
                            $jun = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $jun = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->jul == 1) {
                            $jul = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $jul = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->agu == 1) {
                            $agu = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $agu = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->sep == 1) {
                            $sep = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $sep = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->okt == 1) {
                            $okt = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $okt = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->nov == 1) {
                            $nov = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $nov = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }
                        if ($outstanding[0]->des == 1) {
                            $des = "<span class='glyphicon glyphicon-ok'></span>";
                        }
                        else{
                            $des = "<font color='red'><span class='glyphicon glyphicon-remove'></span>";
                        }


                        ?>
                        2019
                        <table class="table table-bordered" style="background-color: white"> 
                            <thead>
                              <tr>
                                <th class="text-center">Januari</th>
                                <th class="text-center">Februari</th>
                                <th class="text-center">Maret</th>
                                <th class="text-center">April</th>
                                <th class="text-center">Mei</th>
                                <th class="text-center">Juni</th>
                                <th class="text-center">Juli</th>
                                <th class="text-center">Agustus</th>
                                <th class="text-center">September</th>
                                <th class="text-center">Oktober</th>
                                <th class="text-center">November</th>
                                <th class="text-center">Desember</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class="text-center"><?php echo $jan;?></td>
                                <td class="text-center"><?php echo $feb;?></td>
                                <td class="text-center"><?php echo $mar;?></td>
                                <td class="text-center"><?php echo $apr;?></td>
                                <td class="text-center"><?php echo $mei;?></td>
                                <td class="text-center"><?php echo $jun;?></td>
                                <td class="text-center"><?php echo $jul;?></td>
                                <td class="text-center"><?php echo $agu;?></td>
                                <td class="text-center"><?php echo $sep;?></td>
                                <td class="text-center"><?php echo $okt;?></td>
                                <td class="text-center"><?php echo $nov;?></td>
                                <td class="text-center"><?php echo $des;?></td>
                              </tr>

                            </tbody>
                        </table>
                    </div>-->
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-6">
                          <center><h3>Invoice</h3></center>
                          <table class="table table-bordered" style="background-color: white"> 
                            <thead>
                              <tr>
                                <th class="text-center">Bulan</th>
                                <th class="text-center">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php

                              foreach ($invoice as $key) {
                                echo '<tr><td class="text-center">'.$key->Inv_Date.'</td><td class="text-center">'.$key->Status.'</td></tr>';
                              }
                              ?>
                              

                            </tbody>
                        </table>

                        </div>
                        <div class="col-md-6">
                          <center><h3>Cashback</h3></center>
                          <table class="table table-bordered" style="background-color: white"> 
                            <thead>
                              <tr>
                                <th class="text-center">Bulan</th>
                                <th class="text-center">Status</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php

                              foreach ($cashback as $key) {
                                echo '<tr><td class="text-center">'.$key->periode.'</td><td class="text-center">'.$key->status.'</td></tr>';
                              }
                              ?>
                              

                            </tbody>
                        </table>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <table class="table table-bordered"  style="background-color: white ; text-align: center">
                        <thead>
                         <tr>
                           <th>No</th>
                           <th>Invoice</th>
                           <th>Bulan</th>
                           <th>Total Tagihan </th>
                           <th>Invoice Di bayar </th>
                           <th>BY</th>
                           <th>Detail</th>
                         </tr> 
                        </thead>
                        <tbody>
                          <?php $index = 1; foreach ($invoice as $key) {

                          echo "<tr>";
                            echo "<td>".$index++."</td>";
                            echo "<td>".$key->Inv_No."</td>";
                            echo "<td>".$key->Inv_Date."</td>";
                            echo "<td>".$key->Total."</td>";
                            echo "<td>".$key->Dateinsert."</td>";
                            echo "<td>".$key->Created_By."</td>";
                            echo '<td><a id="'.$key->Inv_No.'" type="button" class="modaldetail btn btn-info" data-toggle="modal" data-target="#myModal"  href="" >Detail</a></td>';
                            echo "</tr>";
                          } 
                          ?>
                             
                         
                          <!-- <tr>
                            <td><?php echo $invoice[0]->Inv_No ?></td>
                            <td><?php echo $invoice[0]->Inv_Date ?></td>
                            <td><?php echo $invoice[0]->Total ?></td>
                            <td><?php echo $invoice[0]->Status ?></td>
                            <td><a type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"  href="">Detail</a></td>
                            <td><?php echo $invoice[0]->Created_By ?></td>
                          </tr> -->
                        </tbody>
                      </table>
                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Detail Invoice </h4>
                            </div>
                            <div class="modal-body">
                             <table class="table">
                               <thead>
                                 <tr>
                                   <th>Status</th>
                                   <th>Waktu</th>
                                   <th>Oleh </th>
                                   <th>FEE </th>
                                   <th>SALES </th>
                                 </tr>
                               </thead>
                              <tbody id="detailinv">
                                
                              </tbody>
                             </table>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                </section>

                <?php
               ?>   
            </div>