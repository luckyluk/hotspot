<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

 <div class="content-wrapper">
    <section class="content-header">
       <!--  <h1> Registasi Customer </h1> -->
         <?php echo $breadcrumb; ?>
    </section>
    <div class="col-md-12">
    	<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">New Customer </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
              	
                <div class="form-group">
                  <label for="name">Nama Customer </label>
                  <input type="text" class="form-control" id="name" placeholder="Enter Nama Customer">
                </div>
                <div class="form-group">
                  <label for="subname">Nama Link</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Nama Link">
                </div>
                <div class="form-group">
                  <label for="address"> Alamat</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>
                <div class="form-group">
                  <label for="city">Kota </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter kota">
                </div>
                <div class="form-group">
                  <label for="subname">Provinsi </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                  <label for="pic_name">Nama PIC  </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                  <label for="ktp">No KTP  </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                  <label for="subname">No handphone or Whatapps  </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="08xxxxxxxxxx">
                </div>
                <div class="form-group">
                  <label for="subname">No Whatapps  </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="08xxxxxxxxxx">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal Registasi </label>
                  <input type="date" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Start Biling</label>
                  <input type="date" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nomber FAB</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" >
                </div>
                <hr>
                <div class="form-group">
                  <label for="address"> Biaya Installasi </label>
                  <input type="number" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                  <label for="address"> Biaya Bulanan </label>
                  <input type="number" class="form-control" id="exampleInputEmail1" >
                </div>
                <div class="form-group">
                  <label for="address"> Product  </label>
                  <select class="form-control">
                    <option>Dedicated</option>
                    <option>SOHO</option>
                    <option>Retail </option>
                    
                  </select>
                </div>
                <div class="form-group">
                  <label for="address"> Bandwith  </label>
                  <select class="form-control">
                    <option>5 Mbps</option>
                    <option>10 Mbps</option>
                    <option>20 Mbps </option>
                    <option>50 Mbps </option>
                    <option>100 Mbps </option>
                    
                  </select>
                </div>
                <hr>
                 <div class="form-group">
                  <label for="address"> Username PPOE </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" >
                </div>
                 <div class="form-group">
                  <label for="address"> Password PPOE </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" >
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
    </div>
</div>