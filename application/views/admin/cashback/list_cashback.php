<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
$(document).ready(function() {
	get_list_mitra();
	get_list_customer();
	$('#filterMitra').click(function(event) {
		/* Act on the event */
		window.location.replace(api+'admin/cashback?mitraid='+$('#namamitra').val()+'&custid='+$('#namacust').val()+'&periode='+$('#date').val());
	});
	$('#checkall').change(function() {
	     if(this.checked) {
     		console.log('checked');
     		$(':checkbox').each(function() {
            this.checked = true;                        
        });
	     }
	     else{
	     	console.log('unchecked');
	     	$(':checkbox').each(function() {
            this.checked = false;                        
        });
	     }
	 });
});
</script>
<div class="content-wrapper">
                <section class="content-header">
                    <h1>List Cash Back </h1>
                    <!-- <?php echo $breadcrumb; ?> -->
                    <div class="col-md-12">
                      <div class="col-md-3">
                        <select id="namamitra" style="width:100%">
                          <option>Nama Mitra</option>
                          <option>Iwan</option>
                        </select>
                      </div>
                      
                      <div class="col-md-3">
                        <select id="namacust" style="width:100%">
                          <option>Nama Customer</option>
                          
                        </select>
                      </div>
                      <div class="col-md-3">
                        <select id="date" style="width:100%">
                          <option>Bulan</option>
                          
                        </select>
                      </div>
                      <button id="filterMitra" class="btn btn-primary col-md-1"><i class="fa fa-search"></i></button>
                    </div>
                </section>
                <section class="content">

                	<div class="row">
	                	<div class="col-md-12" >
					          <div class="box">
					            <div class="box-body">
					            <form method="post" action="<?php echo base_url('admin/cashback/update') ?>">
					            <input type="hidden" name="arraycb" id="arraycb">
					              <table class="table table-hover">
				                  <th style="width: 10px">No</th>
				                  <th>No mitra </th>
				                  <th>Nama Mitra </th>
				                  <th>Nama Customer </th>
				                  <th>Cash Back  </th>
				                  <th>Periode  </th>
				                  <th>Status </th>
				                  <th>Create by </th>
				                  <th>Create Date  </th>
				                  <th>Actions
				                  	<br><center><input type='checkbox' id="checkall"></center>
				                  </th>
				                </tr>
				                 </thead>
				                 <tbody>
				                <?php $totalcashback = ''; $index = 1;foreach ($cashback_all as $key){

				                		$cashback = number_format($key->cashback , 0, ',', '.');

				                		  echo "<tr>";
                                          echo "<td>".$index++."</td>";
                                          echo "<td>".$key->no."</td>";
                                          echo "<td>".$key->nama_mitra."</td>";
                                          echo "<td>".$key->nama_customer."</td>";
                                          echo "<td>".$cashback."</td>";
                                          echo "<td>".$key->periode."</td>";
                                          echo "<td>".$key->status."</td>";
                                          echo "<td>".$key->created_by."</td>";
                                          echo "<td>".$key->created_date."</td>";
                                          echo "<td><center><input class='arraycashback' type='checkbox' value='".$key->no."'></center></td>";
                                          //echo "<td>
                                          //<a id='modal' href='' role='button' class='btn btn-app' //data-target='#modal-primary' data-toggle='modal'><i class='fa fa-edit' ></i>edit</a></td>";
                                      
                                          echo "</tr>";

                                          // echo "</tr>";
                                          $totalcashback += $key->cashback;
				                }?> 
				                
				              </tbody></table>
				              <h4><b>Update Status</b></h4>
				              <select name="status" class="form-control">
				              	<option value="created">created</option>
				              	<option value="submitted">submitted</option>
				              	<option value="closed">closed</option>
				              </select>
				              <br>
				              <input type="submit" class="btn btn-primary">
				              </form>
				              	<div><p align="left" style="font-size: 24px;"> Total Cashback : 
					            <?php echo number_format($totalcashback , 0, ',', '.'); ?>
					        	</p>
					            </div>
				            </div>
				            
				            <!-- /.box-body -->
				            <!--<div class="box-footer clearfix">
				              <ul class="pagination pagination-sm no-margin pull-right">
				                <li><a href="#">«</a></li>
				                <li><a href="#">1</a></li>
				                <li><a href="#">2</a></li>
				                <li><a href="#">3</a></li>
				                <li><a href="#">»</a></li>
				              </ul>
				            </div>-->
				            <div class="modal fade" id="modal-primary" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			                  <div class="modal-dialog" role="document">
			                      <div class="modal-content">
			                        <form >
			                          <div class="modal-header">
			                              <h5 class="modal-title" id="myModalLabel">
			                                  Detail User
			                              </h5> 
			                              <button type="button" class="close" data-dismiss="modal">
			                                  <span aria-hidden="true">×</span>
			                              </button>
			                          </div>
			                          <div class="modal-body">
			                              
			                                  <div class="form-group">
			                                       <input type="hidden" id="custid" value="">
			                                      <label for="exampleInputEmail1">
			                                          No. Cashback 
			                                      </label>
			                                      <input type="text" name ="no_inv" class="form-control" id="no_inv" />
			                                  </div>
			                                  <div class="form-group">
			                                      <label for="exampleInputEmail1">
			                                          Update Status
			                                      </label><br>
			                                      <select name="statuscasback" id="statuscasback" class="form-control">
			                                        <option value="Dibayar">Sudah Di Bayar </option>
			                                        <option value="Pengajuan">Pengajuan</option>
			                                      </select>
			                                      
			                                  </div>
			                                  
			                                  
			                            </div>
			                          <div class="modal-footer">
			                               
			                              <button id="submitstatusinv" type="submit" class="btn btn-primary">
			                                      Submit
			                              </button>
			                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
			                                  Close
			                              </button>
			                          </div>
			                        </form>
			                      </div>
			                    </div>
			                  </div>
				          </div>
				        </div>

                </section>
</div>