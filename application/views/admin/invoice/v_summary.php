<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
              <section class="content-header">
                  <h3>Invoice Summary</h3>

              </section>
              <section class="content">
                <div class="col-md-12">
                  <div class="col-md-3">
                    <select id="nama" style="width:100%">
                      <option>Nama Akun</option>
                      <option>Iwan</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <select id="subnama" style="width:100%">

                    </select>
                  </div>
                  <div class="col-md-3">
                    <select id="date" style="width:100%">
                      <option>Bulan Invoice</option>
                      
                    </select>
                  </div>
                  <div class="col-md-3">
                    <select id="produk" style="width:100%">
                      <option>Nama Produk</option>
                      <option>Dedicated</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                      <div class="box">                               
                        <div class="box-body">
                          <table class="table table-hover">
                            <thead>
                              <tr>
                                <th scope="col">No. Invoice</th>
                                <th scope="col">Nama Akun</th>
                                <th scope="col">Nama Sub Akun</th>
                                <th scope="col">Periode</th>
                                <th scope="col">Tagihan</th>
                                <th scope="col">Restitusi</th>
                                <th scope="col">Diskon</th>
                                <th scope="col">Total (Inc PPN)</th>
                                <th scope="col">Sub Produk</th>
                                <th scope="col">Detail</th>
                              </tr>
                            </thead>
                            <tbody id="invlist">

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            

               <?php
               ?>
            </div>

