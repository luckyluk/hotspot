<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Whatsapp Status</h1>
                  <!--   <?php echo $breadcrumb; ?>
 -->                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">                               
                                 <div class="box-body">
                                  <table class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Pesan</th>
                                        <th scope="col">No  Handphone</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Kirim</th>
                                        <th scope="col">File</th>
                                        <th scope="col">Divre</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                     <?php foreach ($whatsapp as $key) {
                                      if ($key->status == "2") {
                                      $status ="Send";  
                                      }
                                       if ($key->status == "1") {
                                      $status ="Sending";  
                                      }
                                      if ($key->status == "3") {
                                      $status ="Fails";  
                                      }
                                      if ($key->status == "4") {
                                      $status ="parsing process";  
                                      }
                                      echo "<tr>";
                                      echo "<td>".$key->id."</td>";
                                      echo "<td>".$key->pesan."</td>";
                                      echo "<td>".$key->no_wa."</td>";
                                      echo "<td>".$status."</td>";
                                      echo "<td>".$key->kirim."</td>";
                                      echo "<td>".$key->file."</td>";
                                      echo "<td>".$key->divre."</td>";
                                      echo "</tr>";
                                     }?>
                                    </tbody>
                                  </table>
                              </div>
                            </div>
                          </div>
                         </div>
                         </section> 

                <div class="modal fade" id="modal-container-501528" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form>
                          <div class="modal-header">
                              <h5 class="modal-title" id="myModalLabel">
                                  Detail User
                              </h5> 
                              <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">×</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              
                                  <div class="form-group">
                                      <input type="hidden" id="bulan" value="">
                                      <input type="hidden" id="nama" value="">
                                       <input type="hidden" id="custid" value="">
                                      <label for="exampleInputEmail1">
                                          No. Invoice
                                      </label>
                                      <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Update Status
                                      </label><br>
                                      <select name="statusinv" id="statusinv">
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="CLOSED">CLOSED</option>
                                      </select>
                                  </div>
                                  
                            </div>
                          <div class="modal-footer">
                               
                              <button id="submitstatusinvretail" type="submit" class="btn btn-primary">
                                      Submit
                              </button>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                  Close
                              </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
            

               <?php
               ?>
            </div>
