<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gooptix | Print Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://172.31.12.154/dashboard_info/assets/frameworks/adminlte/css/adminlte.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style type="text/css">
    .paper {

   width: 842px;
   display: inline-block;
   margin: 0 auto;

    }
  </style>
</head>
<body class="paper" style="font-size: 14px">
<div class="wrapper" style="">
  <!-- Main content -->
  <section class="" style="width:842px">

    <?php
                    function tgl_indo($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    } ?>
                    <?php
                    function tgl_indo_1($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    }

                    $kodeunik = substr($inv[0]->Inv_No, -3);
                     ?>
    <!-- title row -->
              <div class="row">
                <div class="">


            <!-- Main content -->
            <div class="">
              <!-- title row -->
              <div class="row">
                <div class="">
                  
                    <h1 class="pull-right" style="margin-right: 80px ;">INVOICE</h1>
                   <img src="<?php echo base_url($frameworks_dir . '/img/logo.png'); ?>" style="width: 20%; padding: 20px">
                  <br>
                </div>
                <!-- /.col -->
              </div>
              <table>
                <tr>
                  <td style="padding:10px">From
                                <address>
                                  <strong>PT Solusi Prima Connectivist</strong><br>
                                   Villa Pasir Wangi No.A-9<br>Jl. Cigending, Ujung Berung - Bandung 40611<br>
                                  Phone: (022) 30500873 <br>
                                  Email: noc@gooptix.id
                                </address></td>
                  <td style="padding:10px">To
                                <address>
                                  <strong><?php echo $inv[0]->Account_Name; ?></strong><br>
                                  Nama Site   : <?php echo $inv[0]->Account_Sub_Name; ?> <br>
                                  Alamat Site : <?php echo $inv[0]->Account_Sub_Address; ?><br>
                                </address></td>
                  <td style="padding:10px"><b>Invoice  No </b><br><?php echo $inv[0]->Inv_No; ?>
                                <br> 
                                <b>Tanggal  :</b> <?php echo tgl_indo($inv[0]->Inv_Date);?><br>
                                <b>Jatuh Tempo : </b> 20 <?php echo tgl_indo_1($inv[0]->Inv_Date); ?><br></td>
                </tr>
              </table>


              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>NO</th>
                      <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>Pembayaran Layanan / Service : <br/> <strong>GO OPTIX  <?php echo $inv[0]->Sub_Product ?> <?php echo $inv[0]->Bandwidth; ?>  Mbps </strong>
                      <br/> Biaya Bulanan Periode  <?php echo tgl_indo_1($inv[0]->Inv_Date); ?>
                      <?php 
                        if( is_null($inv[0]->Note)){
                          $note = "";
                        }
                        else{
                          $note = "Note :".$inv[0]->Note;
                        }
                        ?>
                      <br/> <?php echo $note?>
                      <!--Note :  <?php echo $_POST["note"]; ?> --> 
                      </td>
                    </tr>
                   
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
              <table>
                <tr>
                  <td style="padding:10px">
                    <p class="lead">Tatacara Pembayaran:</p>
                  

                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                      Pembayaran harap dikirim atau di transfer ke : <br/>
                      <pre>
                      Bank Mandiri Cabang Jakarta Alaydrus <br/>
                      Atas Nama      : PT Solusi Prima Connectivist<br/>
                      No. Rekening   : 121-00-1351358-9<br/>
                      </pre>
                      Mohon untuk konfrimasi ke :<br/>CS Gooptix 0811-2292-139 (WA)
                    </p>
                  </td>
                  <td style="padding:10px">
                    <p class="lead"> Jatuh Tempo 20 <?php echo tgl_indo_1($inv[0]->Inv_Date); ?></p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <td>Subtotal</td>
                        <td>: Rp.<?php 
                        $subtotal = $inv[0]->Subtotal-$inv[0]->Discount_Val-$inv[0]->Other_Val;

                        echo number_format($subtotal, 0, ',', '.'); ?></td>
                      </tr>
                      <tr>
                        <td>Ppn 10% </td>
                        <td>: Rp.<?php echo number_format($inv[0]->PPN, 0, ',', '.'); ?></td>
                      </tr>
                     
                      <tr>
                        <td>Total</td>
                        <td>: Rp.<?php echo number_format($inv[0]->Total, 0, ',', '.'); ?></td>
                      </tr>
                      <tr>
                        <td>Kode Unik</td>
                        <td>: <?php  echo (int)$kodeunik; ?></td>
                      </tr>
                      <tr style="font-size:16px;background-color: rgba(0, 0, 0, 0.05)">
                        <th>Jumlah Transfer</th>
                        <th>:Rp.<?php
                          $transfer = floor($inv[0]->Total/1000)*1000 + (int)$kodeunik;
                          //$transfer = $inv[0]->Total + (int)$kodeunik;
                         echo number_format($transfer, 0, ',', '.'); ?></th>
                      </tr>
                    </table>
                  </td>
                  <td style="padding:10px">
                    <p style="text-align: center;">Bandung, <?php echo tgl_indo($inv[0]->Inv_Date); ?> <br>
                      <?php 
                      if($inv[0]->Sub_Product == 'CORPORATE'){
                        echo "<br><br><br>";
                      }
                      else{
                        $url = base_url($frameworks_dir . '/img/ttddara.png');
                        echo '<img src="'.$url.'" style="width: 250px;" align="middle">';
                      }
                      ?>
                  </p>
                  <p align="center">Dara Lena <br> Finance</p>
                  </td>
                </tr>
              </table>
              <hr>
              <div style="margin-left:15px">Syarat dan Kondisi : 
                <br>- Mohon transfer sesuai dengan jumlah nominal transfer menggunakan kode unik untuk mempermudah validasi pembayaran
                <br>- Apabila ada kesalahan jumlah transfer atau memerlukan informasi lainnya mohon menghubungi CS Gooptix 0811-2292-139 (WA)
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

                </section>
                


</div>
<!-- ./wrapper -->
</body>
</html>
