<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gooptix | Print Invoice Dedicated </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://172.31.12.154/dashboard_info/assets/frameworks/adminlte/css/adminlte.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();document.title='<?php echo $inv[0]->Inv_Date; echo "-";echo $inv[0]->Account_Name ;echo "-";echo $inv[0]->Account_Sub_Name ;  ?>';">
<div class="wrapper">
  <?php
                    function tgl_indo($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    } ?>
                    <?php
                    function tgl_indo_1($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    } ?>
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
              <div class="row">
                <div class="col-12">


            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row" style="padding: 2%">
                <div class="col-12">
                  
                    <h1 class="pull-right">INVOICE</h1>
                   <img class="pull-left" src="<?php echo base_url($frameworks_dir . '/img/logo.png'); ?>" style="width: 20%;">
                  <br>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>PT Solusi Prima Connectivist</strong><br>
                     Villa Pasir Wangi No.A-9<br>Jl. Cigending, Ujung Berung - Bandung 40611<br>
                    Phone: (022) 30500873 <br>
                    Email: noc@gooptix.id
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong><?php echo $inv[0]->Account_Name; ?></strong><br>
                    Nama Site   : <?php echo $inv[0]->Account_Sub_Name; ?> <br>
                    Alamat Site : <?php echo $inv[0]->Account_Sub_Address; ?><br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice </b><br><?php echo $inv[0]->Inv_No; ?>
                  <br> 
                  <b>Tanggal  :</b> <?php echo tgl_indo($inv[0]->Inv_Date);?><br>
                  <b>Jatuh Tempo : </b> 20 <?php echo tgl_indo_1($inv[0]->Inv_Date); ?><br>
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>NO</th>
                      <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>Pembayaran Layanan / Service : <br/> <strong>GO OPTIX  <?php echo $inv[0]->Sub_Product ?> <?php echo $inv[0]->Bandwidth; ?>  Mbps </strong>
                      <br/> Biaya Bulanan Periode  <?php echo tgl_indo_1($inv[0]->Inv_Date); ?>
                      <?php 
                        if( is_null($inv[0]->Note)){
                          $note = "";
                        }
                        else{
                          $note = "Note :".$inv[0]->Note;
                        }
                        ?>
                      <br/> <?php echo $note?>
                      <!--Note :  <?php echo $_POST["note"]; ?> --> 
                      </td>
                    </tr>
                   
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row" style="padding: 2%">
                <!-- accepted payments column -->
                <div class="col-md-4">
                  <p class="lead">Tatacara Pembayaran:</p>
                  

                  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Pembayaran harap dikirim atau di transfer ke rekening <br/>
                    Bank Mandiri Cabang Jakarta Alaydrus<br>
                    Atas Nama : PT Solusi Prima Connectivist<br>
                    No. Rekening : 121-00-1351358-9<br>
                    Pembayaran baru dianggap sah setelah Cek / Giro dicairkan<br/>
                    Sesudah pembayaran mohon untuk konfrimasi <br/>ke CS Gooptix 0811-2292-139
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="lead"> Jatuh Tempo 20 <?php echo tgl_indo_1($inv[0]->Inv_Date); ?></p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>Rp.<?php 
                        $subtotal = $inv[0]->Subtotal-$inv[0]->Discount_Val-$inv[0]->Other_Val;

                        echo number_format($subtotal, 0, ',', '.'); ?></td>
                      </tr>
                      <tr>
                        <th>Ppn 10% : </th>
                        <td>Rp.<?php echo number_format($inv[0]->PPN, 0, ',', '.'); ?></td>
                      </tr>
                     
                      <tr>
                        <th>Total:</th>
                        <td><strong>Rp.<?php echo number_format($inv[0]->Total, 0, ',', '.'); ?></strong></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
                 <div  class="col-md-4">
                   <p style="text-align: center;">Bandung, <?php echo tgl_indo($inv[0]->Inv_Date); ?> </p><br><br><br>
                   <!--<img  src="<?php echo base_url($frameworks_dir . '/img/ttddara.png'); ?>" style="width: 250px;margin-top: -110px;margin-left: 20px;margin-bottom: -25px;" align="middle">--><br><br><br>
                  
                  <p style="text-align: center;"> Dara Lena <br> Finnace</p>
                </div>
              </div>
              <!-- /.row -->

              
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
                </section>


</div>
<!-- ./wrapper -->
</body>
</html>
