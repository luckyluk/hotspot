<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Invoice</h1>
                  <!--   <?php echo $breadcrumb; ?> -->
                </section>
                <div class="col-md-12">
                      <div class="col-md-3">
                        <select id="nama" style="width:100%">
                          <option>Nama Akun</option>
                          <option>Iwan</option>
                        </select>
                      </div>
                      
                      <div class="col-md-3">
                        <select id="date" style="width:100%">
                          <option>Bulan Invoice</option>
                          
                        </select>
                      </div>
                      <div class="col-md-3">
                        <select id="city" style="width:100%">
                          <option>Kota </option>
                          
                        </select>
                      </div>
                      <button id="filterinvoice" class="btn btn-primary col-md-1"><i class="fa fa-search"></i></button>
                    </div>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">                               
                                 <div class="box-body">
                                  <table class="table table-hover ">
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">No. Invoice</th>
                                        <th scope="col">Kota</th>
                                        <th scope="col">Nama Akun</th>
                                       <!--  <th scope="col">Nama Sub Akun</th> -->
                                        <!-- <th scope="col">Periode</th>
                                        <th scope="col">Tagihan</th>
                                        <th scope="col">Restitusi</th>
                                        <th scope="col">Diskon</th> -->
                                        <th scope="col">Total (Inc PPN)</th>
                                        <!-- <th scope="col">Status Invoice</th> -->
                                        <th scope="col">Tanggal Close Invoce</th>
                                        <th scope="col">Butik Bayar </th>

                                        <!-- <th scope="col">Print</th> -->
                                        
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php $index = 1;foreach ($invoice_all as $key) {
                                          # code...
                                        // if($key->Status == "CREATED"){
                                        //   $icon = "<a id='modal-501528' href='#modal-container-501528' role='button' class='btn btn-app bg-green' data-toggle='modal'><i class='fa fa-check-square'></i>";
                                        // }
                                        // else{
                                        //    $icon = "<a id='modal-501528' href='#modal-container-501528' role='button' class='btn btn-app bg-yellow' data-toggle='modal'><i class='fa fa-plane'></i>";
                                        // }

                                          echo "<tr>";
                                          echo "<td>".$index++."</td>";
                                          echo "<td>".$key->Inv_No."</td>";
                                          echo "<td>".$key->City."</td>";
                                          echo "<td>".$key->Account_Name."- ".$key->Account_Sub_Name."</td>";
                                          // echo "<td>".$key->Account_Sub_Name."</td>";
                                          
                                          // echo "<td>".number_format($key->Subtotal, 0, ',', '.')."</td>";
                                          // echo "<td>".number_format($key->Other_Val, 0, ',', '.')."</td>";
                                          // echo "<td>".number_format($key->Discount_Val, 0, ',', '.')."</td>";
                                          echo "<td>".number_format($key->Total, 0, ',', '.')."</td>";
                                          
                                           // echo "<td>".date_format()."</td>";
                                          // echo "<td>".$key->Account_Sub_Name."</td>";
                                          echo "<td>".$key->Inv_Date."</td>";
                                          echo "<td>".$key->Status."</a>
                                          <a id='modal-501528' href='".site_url('/assets/upload/buti_bayar')."/".$key->Inv_No."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-file'></i>Bukti Bayar</a></td>";
                                          echo "</tr>";
                                      }
                                      ?> 
                                    </tbody>
                                  </table>

                              </div>
                            </div>
                          </div>
                         </div>
                         </section> 

                <div class="modal fade" id="modal-container-501528" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form >
                          <div class="modal-header">
                              <h5 class="modal-title" id="myModalLabel">
                                  Bukti bayar 
                              </h5> 
                              <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">×</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              
                                  <div class="form-group">
                                       
                                      <img src=".site_url('/assets/upload/buti_bayar').jpg">
                                  </div>
                                                                   
                            </div>
                          <div class="modal-footer">
                               
                             <!--  <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                      Submit
                              </button> -->
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                  Close
                              </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
            

               <?php
               ?>
            </div>
