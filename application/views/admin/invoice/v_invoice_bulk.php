<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Invoice</h1>
                    <?php echo $breadcrumb; ?>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">                               
                                 <div class="box-body">
                                  <table class="table table-hover ">
                                    <thead>
                                      <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Nama Akun</th>
                                        <th scope="col">Produk</th>
                                        <th scope="col">Periode</th>
                                        <th scope="col">Status Invoice</th>
                                        <!-- <th scope="col">Print</th> -->
                                        
                                      </tr>
                                    </thead>
                                    <tbody>
                                      
                                      <?php
                                      $index = 1;
                                        foreach ($invoice_all as $key) {
                                          # code...
                                          $icon = "<a id='modal-501528' href='#modal-container-501528' role='button' class='btn btn-app bg-green' data-toggle='modal'><i class='fa fa-check-square'></i>";
                                          $pecahkan = explode('-', $key->Inv_Date);
                                          //print_r($pecahkan);
                                          $href = "preview_bulk/?name=".$key->Account_Name."&product=".$key->Sub_Product."&tahun=".$pecahkan[0]."&bulan=".$pecahkan[1];
                                          echo "<tr>";
                                          echo "<td>".$index++."</td>";
                                          echo "<td>".$key->Account_Name."</td>";
                                          echo "<td>".$key->Sub_Product."</td>";
                                          echo "<td>".$key->Inv_Date."</td>";
                                          echo "<td class='col-md-4'>".$icon." ".$key->Status."</a>
                                          <a id='modal-501528' href='".$href."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-file'></i>Invoice</a><a id='modal-501528' href='invoice/print_kwitansi/' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-print'></i>Kwitansi</a></td>";
                                          echo "</tr>";
                                      }
                                      ?> 
                                    </tbody>
                                  </table>

                              </div>
                            </div>
                          </div>
                         </div>
                         </section> 

                <div class="modal fade" id="modal-container-501528" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form >
                          <div class="modal-header">
                              <h5 class="modal-title" id="myModalLabel">
                                  Detail User
                              </h5> 
                              <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">×</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              
                                  <div class="form-group">
                                       <input type="hidden" id="custid" value="">
                                      <label for="exampleInputEmail1">
                                          No. Invoice
                                      </label>
                                      <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Update Status
                                      </label><br>
                                      <select name="statusinv" id="statusinv">
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="CLOSED">CLOSED</option>
                                      </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputFile">Bukti Bayar </label>
                                    <input type="file" id="">
                                  </div>
                                  
                            </div>
                          <div class="modal-footer">
                               
                              <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                      Submit
                              </button>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                  Close
                              </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
            

               <?php
               ?>
            </div>
