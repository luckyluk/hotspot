<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gooptix | Print Kwitansi</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://172.31.12.154/dashboard_info/assets/frameworks/adminlte/css/adminlte.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body onload="window.print();document.title='<?php echo $inv[0]->Inv_Date; echo "-";echo $inv[0]->Account_Name ;echo "-";echo $inv[0]->Account_Sub_Name ;  ?>';">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice" style="padding:30px">
    <!-- title row -->
              <div class="row">
                <div class="col-12">
                  
                    <h1 class="pull-right" style="padding: 20px">KWITANSI</h1>
                   <img class="pull-left" src="http://172.31.12.154/dashboard_info/assets/frameworks/img/logo.png" style="width: 20%  ; padding: 20px">
                  <br>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-9 invoice-col">
                  From
                  <address>
                    <strong>PT Solusi Prima Connectivist</strong><br>
                     Villa Pasir Wangi No.A-9<br>Jl. Cigending, Ujung Berung - Bandung 40611<br>
                    Phone: (022) 30500873 <br>
                    Email: noc@gooptix.id
                  </address>
                </div>
                <!-- /.col -->
                
                <!-- /.col -->
                <div class="col-sm-3 invoice-col">
                  <b>Invoice No : </b><?php echo $inv[0]->Inv_No; ?>
                  <br> 
                  <b>Tanggal  :</b> 01 <?php echo tgl_indo_1(date("Y-m")); ?><br>
                  <b>Jatuh Tempo : </b> 20 <?php echo tgl_indo_1(date("Y-m")); ?><br>
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->

              <?php
                    function tgl_indo($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    } 
                    function tgl_indo_1($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    }
class moneyFormat {
 
    public function rupiah ($angka) {
        $rupiah = number_format($angka ,0, ',' , '.' );
        return $rupiah;
    }
 
    public function terbilang ($angka) {
        $angka = (float)$angka;
        $bilangan = array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas');
        if ($angka < 12) {
            return $bilangan[$angka];
        } else if ($angka < 20) {
            return $bilangan[$angka - 10] . ' Belas';
        } else if ($angka < 100) {
            $hasil_bagi = (int)($angka / 10);
            $hasil_mod = $angka % 10;
            return trim(sprintf('%s Puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
        } else if ($angka < 200) { return sprintf('Seratus %s', $this->terbilang($angka - 100));
        } else if ($angka < 1000) { $hasil_bagi = (int)($angka / 100); $hasil_mod = $angka % 100; return trim(sprintf('%s Ratus %s', $bilangan[$hasil_bagi], $this->terbilang($hasil_mod)));
        } else if ($angka < 2000) { return trim(sprintf('Seribu %s', $this->terbilang($angka - 1000)));
        } else if ($angka < 1000000) { $hasil_bagi = (int)($angka / 1000); $hasil_mod = $angka % 1000; return sprintf('%s Ribu %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod));
        } else if ($angka < 1000000000) { $hasil_bagi = (int)($angka / 1000000); $hasil_mod = $angka % 1000000; return trim(sprintf('%s Juta %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
        } else if ($angka < 1000000000000) { $hasil_bagi = (int)($angka / 1000000000); $hasil_mod = fmod($angka, 1000000000); return trim(sprintf('%s Milyar %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
        } else if ($angka < 1000000000000000) { $hasil_bagi = $angka / 1000000000000; $hasil_mod = fmod($angka, 1000000000000); return trim(sprintf('%s Triliun %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
        } else {
            return 'Data Salah';
        }
    }
}

              ?>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    
                    <tbody>
                    <tr>
                      <td>Sudah Terima Dari :  <strong> <?php echo $inv[0]->Account_Name; ?></strong> <br> 
                        Untuk Pembayaran : <strong><?php echo $inv[0]->Account_Sub_Name." Periode ".tgl_indo_1(date("Y-m")); ?> </strong>                      <br/>
                       Banyaknya Uang : <br/> 
                       <?php 
$money = new moneyFormat;
$terbilang = $money->terbilang($inv[0]->Total);
echo PHP_EOL;
echo "<h3 style='text-align:right; padding: 20px'>".$terbilang." Rupiah</h3>";
                      ?><hr>
                       <h3 style=" padding: 5px">Rp. <?php echo number_format($inv[0]->Total, 0, ',', '.'); ?>,-</h3>
                       <hr>
                      
                                          
                      </td>
                    </tr>
                   
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <!--<p class="lead">Note:</p>
                   <img src="../../dist/img/credit/visa.png" alt="Visa">
                  <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                  <img src="../../dist/img/credit/american-express.png" alt="American Express">
                  <img src="../../dist/img/credit/paypal2.png" alt="Paypal"> -->

                  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Pembayaran harap dikirim atau di transfer ke rekening <br/>
                    Bank Name : Bank Mandiri Cabang Jakarta Alaydrus <br/>
                    Acc. Name : PT Solusi Prima Connectivist<br/>
                    Acc. No   : 121-00-1351358-9<br/>
                    Mohon untuk konfrimasi <br/>ke CS Gooptix 0811-2292-139(WA)
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-6">
                 <!--  <p class="lead"> <b>Rp.<?php echo number_format($inv[0]->Total, 0, ',', '.'); ?> </b></p> <br/> -->
                  <p style="text-align: center;">Bandung, 01 <?php echo date("F Y"); ?></p>
                  
                  <!--<img class="pull-center" src="<?php echo base_url($frameworks_dir . '/img/gooptix_veny.png'); ?>" style="width: 50%;">-->
                  <br><br><br><br><br><br>
                  <p style="text-align: center;"> Dara Lena<br> Finance</p>
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
