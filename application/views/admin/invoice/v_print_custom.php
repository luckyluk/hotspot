<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gooptix | Print Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="http://172.31.12.154/dashboard_info/assets/frameworks/adminlte/css/adminlte.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<!--<body>-->
  <body onload="window.print();document.title='<?php echo $inv[0]->Inv_Date; echo "-";echo $inv[0]->Account_Name ; ?>';">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">

    <?php
                    function tgl_indo($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    } ?>
                    <?php
                    function tgl_indo_1($tanggal){
                      $bulan = array (
                        1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                      );
                      $pecahkan = explode('-', $tanggal);
                      
                      // variabel pecahkan 0 = tanggal
                      // variabel pecahkan 1 = bulan
                      // variabel pecahkan 2 = tahun
                     
                      return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
                    } ?>
    <!-- title row -->
              <div class="row">
                <div class="col-12">


            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  
                    <h1 class="pull-right" style="padding: 20px ;">INVOICE</h1>
                   <img class="pull-left" src="<?php echo base_url($frameworks_dir . '/img/logo.png'); ?>" style="width: 20%; padding: 20px">
                  <br>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>PT Solusi Prima Connectivist</strong><br>
                     Villa Pasir Wangi No.A-9<br>Jl. Cigending, Ujung Berung - Bandung 40611<br>
                    Phone: (022) 30500873 <br>
                    Email: noc@gooptix.id
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong><?php echo $inv[0]->Account_Name; ?></strong><br>
                  </address>
                  Link
                  <address>
                    <strong><?php echo $inv[0]->Account_Sub_Name; ?></strong><br>
                  </address>
                </div>

                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <?php
                    if($inv[0]->Sub_Product == "RETAIL"){
                      $produk = "HOME";
                    }
                    else{
                      $produk = $inv[0]->Sub_Product;
                    }
                   ?>
                  <b>Periode  :</b> <?php echo tgl_indo_1($inv[0]->Inv_Date);?><br>
                  <b>Tanggal  :</b> <?php echo tgl_indo($inv[0]->Inv_Date);?><br>
                  <b>Jatuh Tempo : </b> 20 <?php echo tgl_indo_1($inv[0]->Inv_Date); ?><br>
                  <b>Produk : </b> <?php echo $produk; ?><br>
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>NO</th>
                      <th>No. Invoice</th>
                      <th>Deskripsi</th>
                      <th>Biaya</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                      $sum = 0;
                      $index = 1;
                      foreach ($inv as $key) {
                       
                      $subtotal = $key->Subtotal-$key->Discount_Val-$key->Other_Val;
                      $sum += $subtotal;
                      echo '<tr>';
                        echo '<td>'.$index++.'</td>';
                        echo '<td>'.$key->Inv_No.'</td>';
                        //echo '<td>'.$key->Account_Sub_Name.'</td>';
                        echo '<td>'.$key->Note;
                        echo '</td>';
                        echo '<td>Rp.'.number_format($subtotal, 0, ',', '.').'</td>';
                      echo '</tr>';
                    } ?> 
                   
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-md-4">
                  <p class="lead">Tatacara Pembayaran:</p>
                  

                  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Pembayaran harap dikirim atau di transfer ke rekening <br/>
                    Bank Name : Bank Mandiri Cabang Jakarta Alaydrus <br/>
                    Acc. Name : PT Solusi Prima Connectivist<br/>
                    Acc. No   : 121-00-1351358-9<br/>
                    Mohon untuk konfrimasi <br/>ke CS Gooptix 0811-2292-139 (WA)
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p class="lead"> Jatuh Tempo 20 <?php echo tgl_indo_1($inv[0]->Inv_Date); ?></p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>Rp.<?php 

                        echo number_format($sum, 0, ',', '.'); ?></td>
                      </tr>
                      <tr>
                        <th>Ppn 10% : </th>
                        <td>Rp.<?php echo number_format($sum*0.1, 0, ',', '.'); ?></td>
                      </tr>
                     
                      <tr>
                        <th>Total:</th>
                        <td>Rp.<?php echo number_format($sum*1.1, 0, ',', '.'); ?></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                   
                   <p style="text-align: center;">Bandung, <?php echo tgl_indo($inv[0]->Inv_Date); ?> <br>
                  <!-- <img class="pull-left" src="http://172.31.12.242/dashboard_info/assets/frameworks/img/gooptix_venny.png" style="width: 20%; padding: 20px"> -->
                  <img  src="<?php echo base_url($frameworks_dir . '/img/ttddara.png'); ?>" style="width: 50%;" align="middle"></p> 
                  
                  <p align="center">Dara Lena <br> Finance</p>
                </div>
              
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <!-- <div class="row no-print">
                <div class="col-md-12">
                  <a href="invoice-print.html" target="_blank" class="btn btn-primary col-md-6"><i class="fa fa-print"></i> Print Invoice </a>
                  <a href="Kwitansi.html" target="_blank" class="btn btn-default col-md-6"><i class="fa fa-print"></i> Print Kwitansi </a>
                 
                </div>
              </div> -->
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
                </section>


</div>
<!-- ./wrapper -->
</body>
</html>
