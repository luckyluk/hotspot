<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Invoice</h1>
                    <?php echo $breadcrumb; ?> <br>
                    <!-- <a class="btn btn-primary" href="http://billing.gooptix.id/admin/invoice/retail?city=bekasi">Bekasi</a> | 
                    <a class="btn btn-primary" href="http://billing.gooptix.id/admin/invoice/retail?city=bandung"> Bandung </a> | 
                    <a class="btn btn-primary" href="http://billing.gooptix.id/admin/invoice/retail?city=kab. bandung"> Kab Bandung </a> | 
                    <a class="btn btn-primary" href="http://billing.gooptix.id/admin/invoice/retail?city=cirebon"> Cirebon </a>  -->                    
                </section>
                   <div class="col-md-12">
                      <div class="form-group col-md-3">
                        <select class="form-control" id="nama" style="width:100%">
                          <option value="">Nama Akun</option>
                        </select>
                      </div>
                      
                      <div class="form-group col-md-3">
                        <select class="form-control" id="date" style="width:100%">
                          <option>Bulan Invoice</option>
                          
                        </select>
                      </div>
                      <div class=" form-group col-md-3">
                        <select class="form-control" id="city" style="width:100%">
                          <option>Kota </option>
                          
                        </select>
                      </div>
                      <button id="filterinvoiceretail" class="btn btn-primary col-md-1"><i class="fa fa-search"></i></button>
                    </div>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">                               
                                 <div class="box-body">
                                  <table class="table table-hover" id="invoice_retail">
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">No. Invoice</th>
                                        <th scope="col">Kota</th>
                                        <th scope="col">Nama Akun</th>
                                        <!-- <th scope="col">Nama Sub Akun</th> -->
                                        <th scope="col">Periode</th>
                                        <th scope="col">Total Tagihan</th>
                                        <th scope="col">Kontak</th>
                                       <!--  <th scope="col">Phone</th -->>
                                        <th scope="col">Print</th>
                                        <th scope="col" style="display: none;">Edit</th>
                                      </tr>
                                    </thead>
                                    <tbody id="emp_body">
                                      <?php $index = 1; foreach ($invoice_all as $key) {
                                          # code...

                                        $pecahkan = explode('-', $key->Inv_Date);
                                        if($key->Status == "CREATED"){
                                          $icon = "<a id='modal-501528' href='#modal-container-501528' role='button' class='btn btn-app bg-green' data-toggle='modal'><i class='fa fa-check-square'></i>";
                                        }
                                        else{
                                          $icon = "<a id='modal-501528' href='#modal-container-501528' role='button' class='btn btn-app bg-yellow' data-toggle='modal'><i class='fa fa-plane'></i>";
                                        }

                                         if ($key->Handphone == null) {
                                          $number = '0';
                                        }
                                        else{
                                          $number = $key->Handphone;
                                        }

                                        
                                        $country_code = '62';
                                        
                                        $new_number = substr_replace($number, ''.$country_code, 0, ($number[0] == '0'));
                                        
                                        if ($key->City == "Bekasi") {
                                          # code...
                                          $hrefwa = 'http://billing.gooptix.id/admin/customer/kirim_wa?nowa='.$new_number.'&pesan=Dear%20Customer%20Gooptix,%0ABerikut%20saya%20sampaikan%20invoice%20bulan%20'.$pecahkan[1].'%20Tahun%20'.$pecahkan[0].'%0A%0Ahttp://billing.gooptix.id/invoice/bekasi/'.$pecahkan[0].'/'.$pecahkan[1].'/'.$key->Inv_No.'.pdf%0ASalam,%0ATeam%20GoOptix%20WA%20:%20087870630688&file='.$key->Inv_No.'.pdf&folder=bekasi/'.$pecahkan[0].'/'.$pecahkan[1];
                                          $pesan = 'Dear%20Customer%20Gooptix,%0ABerikut%20saya%20sampaikan%20invoice%20bulan%20'.$pecahkan[1].'%20Tahun%20'.$pecahkan[0].'%0A%0Ahttp://billing.gooptix.id/invoice/bekasi/'.$pecahkan[0].'/'.$pecahkan[1].'/'.$key->Inv_No.'.pdf%0ASalam,%0ATeam%20GoOptix%20WA%20:%20087870630688';


                                          $linkwa = "<a target='_blank' href='".$hrefwa."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-print'></i>WhatsApp</a>";
                                          $linkemail = "<a id='modal-501528' href='http://172.31.12.242/mail/emailinvoicebekasi.php?tahun=".$pecahkan[0]."&bulan=".$pecahkan[1]."&inv_no=".$key->Inv_No."&email=".$key->Email."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-print'></i>Email</a>";

                                          if($key->Invoice_Category == "monthly" || $key->Invoice_Category == "Monthly"){
                                            $linkinv = "http://billing.gooptix.id/invoice/bekasi/".$pecahkan[0]."/".$pecahkan[1]."/".$key->Inv_No.".pdf";
                                            $td = "<td class='col-md-4'>".$icon." ".$key->Status."</a>
                                          <a id='modal-501528' href='".$linkinv."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-file'></i>Invoice</a><a id='modal-501528' href='print_kwitansi/".$key->Inv_No."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-print'></i>Kwitansi</a>".$linkemail."".$linkwa." <a id='modal-501' href='#modal-container-501' role='button' class='btn btn-app' data-toggle='modal'><i class='fa  fa-file-text-o'></i>BULK</a> <a id='modal-5012' href='#modal-container-5012' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-money'></i>Bukti Bayar </a> </td>";
                                          }
                                          else{
                                            $linkinv = "http://billing.gooptix.id/invoice/bekasi/".$pecahkan[0]."/".$pecahkan[1]."/".$key->Inv_No.".pdf";
                                            $td = "<td class='col-md-4'>".$icon." ".$key->Status."</a>
                                          <a id='modal-501528' href='".$linkinv."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-file'></i>Invoice</a> <a id='modal-501' href='#modal-container-501' role='button' class='btn btn-app' data-toggle='modal'><i class='fa  fa-file-text-o'></i>BULK</a> <a id='modal-5012' href='#modal-container-5012' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-money'></i>Bukti Bayar </a> </td>";
                                          }
                                          

                                        }
                                        else {
                                          $hrefwa = 'http://billing.gooptix.id/admin/customer/kirim_wa?nowa='.$new_number.'&pesan=Dear%20Customer%20Gooptix,%0ABerikut%20saya%20sampaikan%20invoice%20bulan%20'.$pecahkan[1].'%20Tahun%20'.$pecahkan[0].'%0A%0Ahttp://billing.gooptix.id/invoice/'.$pecahkan[0].'/'.$pecahkan[1].'/'.$key->Inv_No.'.pdf%0ASalam,%0ATeam%20GoOptix%20WA%20:%2008112292139&file='.$key->Inv_No.'.pdf&folder='.$pecahkan[0].'/'.$pecahkan[1];
                                          $pesan = 'Dear%20Customer%20Gooptix,%0ABerikut%20saya%20sampaikan%20invoice%20bulan%20'.$pecahkan[1].'%20Tahun%20'.$pecahkan[0].'%0A%0Ahttp://billing.gooptix.id/invoice/'.$pecahkan[0].'/'.$pecahkan[1].'/'.$key->Inv_No.'.pdf%0ASalam,%0ATeam%20GoOptix%20WA%20:%2008112292139';


                                          $linkwa = "<a target='_blank' href='".$hrefwa."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-print'></i>WhatsApp</a>";
                                          $linkemail = "<a id='modal-501528' href='http://172.31.12.242/mail/emailinvoice.php?tahun=".$pecahkan[0]."&bulan=".$pecahkan[1]."&inv_no=".$key->Inv_No."&email=".$key->Email."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-print'></i>Email</a>";
                                          
                                          if($key->Invoice_Category == "monthly" || $key->Invoice_Category == "Monthly"){
                                            $linkinv = "http://billing.gooptix.id/invoice/".$pecahkan[0]."/".$pecahkan[1]."/".$key->Inv_No.".pdf";
                                            $td = "<td class='col-md-4'>".$icon." ".$key->Status."</a>
                                          <a id='modal-501528' href='".$linkinv."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-file'></i>Invoice</a><a id='modal-501528' href='print_kwitansi/".$key->Inv_No."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-print'></i>Kwitansi</a>".$linkemail."".$linkwa." <a id='modal-501' href='#modal-container-501' role='button' class='btn btn-app' data-toggle='modal'><i class='fa  fa-file-text-o'></i>BULK</a> <a id='modal-5012' href='#modal-container-5012' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-money'></i>Bukti Bayar </a> </td>";
                                          }
                                          else{
                                            $linkinv = "http://billing.gooptix.id/invoice/".$pecahkan[0]."/".$pecahkan[1]."/".$key->Inv_No.".pdf";
                                            $td = "<td class='col-md-4'>".$icon." ".$key->Status."</a>
                                          <a id='modal-501528' href='".$linkinv."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-file'></i>Invoice</a> <a id='modal-501' href='#modal-container-501' role='button' class='btn btn-app' data-toggle='modal'><i class='fa  fa-file-text-o'></i>BULK</a> <a id='modal-5012' href='#modal-container-5012' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-money'></i>Bukti Bayar </a> </td>";
                                          }
                                        }

                                          echo "<tr>";
                                           echo "<td>".$index++."</td>";
                                          echo "<td>".$key->Inv_No."</td>";
                                          echo "<td>".$key->City."</td>";
                                          echo "<td>".$key->Account_Name." - ".$key->Account_Sub_Name."</td>";
                                          // echo "<td>".$key->Account_Sub_Name."</td>";
                                          echo "<td>".$key->Inv_Date."</td>";
                                          echo "<td>".number_format($key->Total, 0, ',', '.')."</td>";
                                          echo "<td>".$key->Email." - ".$new_number."</td>";
                                          // echo "<td>".$new_number."</td>";
                                          echo $td;
                                          echo "<td style='display:none'>".$key->Reg_ID."</td>";
                                          echo "</tr>";
                                      }
                                      ?> 
                                    </tbody>
                                  </table>
                              </div>
                            </div>
                          </div>
                         </div>
                         </section> 

                <div class="modal fade" id="modal-container-501528" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                          <?php echo form_open_multipart('admin/invoice/update_invoice_retail');?>
                          <div class="modal-header">
                              <h5 class="modal-title" id="myModalLabel">
                                  Detail User
                              </h5> 
                              <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">×</span>
                              </button>
                          </div>
                          <div class="modal-body">
                                  <div class="form-group">
                                      <input type="hidden" name="bulan" id="bulan" value="">
                                      <input type="hidden" name="nama" id="nama" value="">
                                       <input type="hidden" name="custid" id="custid" value="">
                                      <label for="exampleInputEmail1">
                                          No. Invoice
                                      </label>
                                      <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Update Status
                                      </label><br>
                                      <select class="form-control" name="statusinv" id="statusinv">
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="CLOSED">CLOSED</option>
                                       <!--  -->
                                      
                                      </select>
                                  </div>
                                  <div id="buktibayar">
                                    <div class="form-group">
                                         <input type="hidden" id="custid" value="">
                                        <label for="exampleInputEmail1">
                                            Tanggal Bayar
                                        </label>
                                        <input type="date" name ="tanggalbayar" class="form-control" id="" />
                                    </div>
                                    <div class="form-group">
                                         <input type="hidden" id="custid" value="">
                                        <label for="exampleInputEmail1">
                                            Bukti Bayar
                                        </label>
                                        <input type="file" name="berkas" />
                                    </div>
                                  </div>  
                            </div>
                          <div class="modal-footer">
                               <input type="submit" class="btn btn-primary" name="">
                              <!--<button id="submitstatusinvretail" type="submit" class="btn btn-primary">
                                      Submit
                              </button>-->
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                  Close
                              </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
            
                  <div class="modal fade" id="modal-container-501" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <form >
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">
                                    Detail User
                                </h5> 
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                
                                    <div class="form-group">
                                         <input type="hidden" id="custid" value="">
                                        <label for="exampleInputEmail1">
                                            No. Invoice
                                        </label>
                                        <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                            Update Status
                                        </label><br>
                                        <select name="" id="" class="form-control">
                                          <option value="BULK">BULK</option>
                                        </select>
                                    </div>
                                   
                                    
                              </div>
                            <div class="modal-footer">
                                 
                                <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                        Submit
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>

                  <div class="modal fade" id="modal-container-5012" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <form >
                             
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">
                                   Bukti Bayar 
                                </h5> 
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">
                                     Update Status
                                  </label><br>
                                  <select name="statusinv" id="statusinv" class="form-control">
                                    <option value="CLOSED">CLOSED</option>
                                  </select>
                                </div>
                                
                                    <div class="form-group">
                                         <input type="hidden" id="custid" value="">
                                        <label for="exampleInputEmail1">
                                            No. Invoice
                                        </label>
                                        <input type="text" name ="" class="form-control" id="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                           Nama Customer 
                                        </label><br>
                                        <input type="text" name ="" class="form-control" id="" />
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Metode pembayaran 
                                      </label><br>
                                      <select name="" id="" class="form-control">
                                        <option value="TRANSFER">TRANSFER</option>
                                        <option value="CASH">CASH</option>
                                     
                                      </select>
                                  </div>
                                     <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          NAMA BANK 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                   <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          NAMA BANK 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                   <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          NAMA BANK 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Total Invoice  
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Total Bayar 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputFile">Bukti Bayar </label>
                                    <input type="file" id="">
                                  </div>
                              </div>
                            <div class="modal-footer">
                                 
                                <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                        Submit
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>
               <?php
               ?>
            </div>
