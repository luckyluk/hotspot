<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
              <section class="content-header">
                  <h3>Invoice Log</h3>

              </section>
              <section class="content">
                <div class="col-md-12">
                  <input type="text" id="keyinv1" name="keyinv1" placeholder="Ketik Nomer Invoice" style="width:50%"><br>
                </div>
                <div class="row">
                    <div class="col-md-12"><br>
                      <div class="box">                               
                        <div class="box-body">
                          <table class="table table-hover">
                            <thead>
                              <tr>
                                <th scope="col">No. Invoice</th>
                                <th scope="col">Pengguna</th>
                                <th scope="col">Waktu</th>
                                <th scope="col">Note</th>
                              </tr>
                            </thead>
                            <tbody id="invloglist">

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            

               <?php
               ?>
            </div>

