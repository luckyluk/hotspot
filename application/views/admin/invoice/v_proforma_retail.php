<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    Proforma Invoice

                    <p align="right"><a href="create_retail" class="btn btn-primary">Create Invoice</a></p>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">                               
                                 <div class="box-body">
                                  <table class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">No. Invoice</th>
                                        <th scope="col">Kota</th>
                                        <th scope="col">Nama Akun</th>
                                       <!--  <th scope="col">Nama Sub Akun</th> -->
                                        <th scope="col">Periode</th>
                                        <th scope="col">Tagihan</th>
                                        <th scope="col">Restitusi</th>
                                        <th scope="col">Diskon</th>
                                        <th scope="col">Total (Inc PPN)</th>
                                        <th scope="col">Edit</th>
                                        <th scope="col" style="display: none;">Edit</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php $index = 1; foreach ($proforma_all as $key) {
                                          # code...
                                          $subtotal = number_format($key->Subtotal , 0, ',', '.');
                                          $restitusi = number_format($key->Restitusi , 0, ',', '.');
                                          $diskon = number_format($key->Diskon , 0, ',', '.');
                                          $total = number_format($key->Total , 0, ',', '.');
                                          if($key->City == "Bekasi" || $key->City == "bekasi" || $key->City == "BEKASI"){
                                            if($key->Invoice_Category == "monthly" || $key->Invoice_Category == ""){
                                              $href = "create_retail_one_bekasi/".$key->Invoice_No;
                                            }
                                            else{
                                              $href = "create_retail_instalasi_bekasi/".$key->Invoice_No;
                                            }
                                            
                                          }
                                          else{
                                            if($key->Invoice_Category == "monthly" || $key->Invoice_Category == ""){
                                              $href = "create_retail_one/".$key->Invoice_No;
                                            }
                                            else{
                                              $href = "create_retail_instalasi/".$key->Invoice_No;
                                            }
                                          }
                                          echo "<tr>";
                                          echo "<td>".$index++."</td>";
                                          echo "<td>".$key->Invoice_No."</td>";
                                          echo "<td>".$key->City."</td>";
                                          echo "<td>".$key->Account_Name." - ".$key->Account_Sub_Name."</td>";
                                          // echo "<td>".$key->Account_Sub_Name."</td>";
                                          echo "<td>".$key->Periode."</td>";
                                          echo "<td>".$subtotal."</td>";
                                          echo "<td>".$restitusi."</td>";
                                          echo "<td>".$diskon."</td>";
                                          echo "<td>".$total."</td>";
                                          echo "<td class='col-md-3'><a id='modal-501528' href='#modal-container-501528' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-edit'></i>Edit</a><a href='".$href."' role='button' class='btn btn-app' data-toggle='modal'><i class='fa fa-plus-circle'></i>Create</a></td>";
                                          echo "<td style='display:none'>".$key->Account_No."</td>";
                                          echo "</tr>";
                                      }
                                      ?> 
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </section>

                <div class="modal fade" id="modal-container-501528" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form action="<?php echo base_url(). 'admin/invoice/update_retail'; ?>" method="post">
                          <div class="modal-header">
                              <h5 class="modal-title" id="myModalLabel">
                                  Detail User
                              </h5> 
                              <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">×</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              
                                  <div class="form-group">
                                       <input type="hidden" id="custid" value="">
                                      <label for="exampleInputEmail1">
                                          No. Invoice
                                      </label>
                                      <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                  </div>
                                  <div class="form-group">
                                       
                                      <label for="exampleInputPassword1">
                                          Nama User
                                      </label>
                                      <input type="text" name="nama" class="form-control" id="nama" />
                                  </div>
                                  <div class="form-group">
                                       
                                      <label for="exampleInputPassword1">
                                          No. Account
                                      </label>
                                      <input type="text" name="no_account" class="form-control" id="no_account" />
                                  </div>
                                  <div class="form-group">
                                       
                                      <label for="exampleInputPassword1">
                                          Total Tagihan (Sebelum PPN)
                                      </label>
                                      <input type="text" name="tagihan" class="form-control" id="tagihan" />
                                  </div>
                                  <div class="form-group">
                                       
                                      <label for="exampleInputPassword1">
                                          Restitusi
                                      </label>
                                      <input type="text" name="restitusi" class="form-control" id="restitusi" />
                                  </div>
                                  <div class="form-group">
                                       
                                      <label for="exampleInputPassword1">
                                          Diskon
                                      </label>
                                      <input type="text" name="diskon" class="form-control" id="diskon" />

                                  </div>
                            </div>
                          <div class="modal-footer">
                               
                              <button type="submit" class="btn btn-primary">
                                      Submit
                              </button>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                  Close
                              </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
            

               <?php
               ?>
            </div>
