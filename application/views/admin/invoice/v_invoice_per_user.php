<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>Invoice</h1>
                    <?php echo $breadcrumb; ?>
                </section>
               <div class="col-md-12">
                      <div class="form-group col-md-3">
                        <select class="form-control" id="nama" style="width:100%">
                          <option>Nama Akun</option>
                          <option>Iwan</option>
                        </select>
                      </div>
                      
                      <div class="form-group col-md-3">
                        <select class="form-control" id="date" style="width:100%">
                          <option>Bulan Invoice</option>
                          
                        </select>
                      </div>
                      <div class=" form-group col-md-3">
                        <select class="form-control" id="city" style="width:100%">
                          <option>Kota </option>
                          
                        </select>
                      </div>
                      <button id="filterinvoice" class="btn btn-primary col-md-1"><i class="fa fa-search"></i></button>
                      
                </div>
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">                               
                                 <div class="box-body">
                                  <form method="get" action="<?php echo base_url().'/admin/invoice/preview_per_user'?>">
                                    <input type="hidden" name="arrayinvoice" id="arrayinvoice">
                                  <table class="table table-hover" id="tabelinvoice">
                                    <colgroup><col width="20%"><col width="35%"><col width="40%"></colgroup>
                                    <thead>
                                      <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">No. Invoice</th>
                                        <th scope="col">Periode</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Print</th>                                       
                                      </tr>
                                    </thead>
                                    <tbody id="emp_body">
                                      <?php $index = 1;foreach ($invoice_all as $key) {

                                          echo "<tr>";
                                          echo "<td>".$index++."</td>";
                                          echo "<td>".$key->Inv_No."</td>";
                                          echo "<td>".$key->Inv_Date."</td>";
                                          echo "<td>".number_format($key->Total, 0, ',', '.')."</td>";
                                          echo "<td>".$key->Status."</td>";
                                          echo "<td><input class='arrayinv' type='checkbox' value='".$key->Inv_No."'></td>";
                                          echo "</tr>";
                                      }
                                      ?> 
                                    </tbody>
                                  </table>
                                  <button type="submit">Print</button>
                                  </form>
                              </div>
                            </div>
                          </div>
                         </div>
                         </section> 

                <div class="modal fade" id="modal-container-501528" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form >
                          <div class="modal-header">
                              <h5 class="modal-title" id="myModalLabel">
                                  Detail User
                              </h5> 
                              <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">×</span>
                              </button>
                          </div>
                          <div class="modal-body">
                              
                                  <div class="form-group">
                                       <input type="hidden" id="custid" value="">
                                      <label for="exampleInputEmail1">
                                          No. Invoice
                                      </label>
                                      <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Update Status
                                      </label><br>
                                      <select name="statusinv" id="statusinv" class="form-control">
                                        <option value="DELIVERED">DELIVERED</option>
                                        <option value="CLOSED">CLOSED</option>
                                      </select>
                                      
                                  </div>
                                  
                                  
                            </div>
                          <div class="modal-footer">
                               
                              <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                      Submit
                              </button>
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                  Close
                              </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                
                  <div class="modal fade" id="modal-container-501" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <form >
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">
                                    Detail User
                                </h5> 
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                
                                    <div class="form-group">
                                        <input type="hidden" id="bulan" value="">
                                        <input type="hidden" id="nama" value="">
                                        <input type="hidden" id="custid" value="">
                                        <label for="exampleInputEmail1">
                                            No. Invoice
                                        </label>
                                        <input type="text" name ="no_inv" class="form-control" id="no_inv" />
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                            Update Status
                                        </label><br>
                                        <select name="" id="" class="form-control">
                                          <option value="BULK">BULK</option>
                                        </select>
                                    </div>
                                   
                                    
                              </div>
                            <div class="modal-footer">
                                 
                                <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                        Submit
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>

                  <div class="modal fade" id="modal-container-5012" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <form >
                             
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">
                                   Bukti Bayar 
                                </h5> 
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                  <label for="exampleInputEmail1">
                                     Update Status
                                  </label><br>
                                  <select name="statusinv" id="statusinv" class="form-control">
                                    <option value="CLOSED">CLOSED</option>
                                  </select>
                                </div>
                                
                                    <div class="form-group">
                                         <input type="hidden" id="custid" value="">
                                        <label for="exampleInputEmail1">
                                            No. Invoice
                                        </label>
                                        <input type="text" name ="" class="form-control" id="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                           Nama Customer 
                                        </label><br>
                                        <input type="text" name ="" class="form-control" id="" />
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Metode pembayaran 
                                      </label><br>
                                      <select name="" id="" class="form-control">
                                        <option value="TRANSFER">TRANSFER</option>
                                        <option value="CASH">CASH</option>
                                     
                                      </select>
                                  </div>
                                     <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          NAMA BANK 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                   <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          NAMA BANK 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                   <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          NAMA BANK 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Total Invoice  
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">
                                          Total Bayar 
                                      </label><br>
                                     <input type="text" name ="" class="form-control" id="" />
                                  </div>
                                  <div class="form-group">
                                    <label for="exampleInputFile">Bukti Bayar </label>
                                    <input type="file" id="">
                                  </div>
                              </div>
                            <div class="modal-footer">
                                 
                                <button id="submitstatusinv" type="submit" class="btn btn-primary">
                                        Submit
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    Close
                                </button>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>
            
            </div>