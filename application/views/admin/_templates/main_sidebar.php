<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <aside class="main-sidebar">
                <section class="sidebar">
<?php if ($admin_prefs['user_panel'] == TRUE): ?>
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url($avatar_dir . '/m_001.png'); ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $user_login['firstname'].$user_login['lastname']; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo lang('menu_online'); ?></a>
                        </div>
                    </div>

<?php endif; ?>
<?php if ($admin_prefs['sidebar_form'] == TRUE): ?>
                    <!-- Search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="<?php echo lang('menu_search'); ?>...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>

<?php endif; ?>
                    <!-- Sidebar menu -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?php echo site_url('auth/login'); ?>">
                                <i class="fa fa-home text-primary"></i> <span><?php echo lang('menu_access_website'); ?></span>
                            </a>
                        </li>

                        <!--<li class="header text-uppercase"><?php echo lang('menu_main_navigation'); ?></li>
                        <li class="<?=active_link_controller('dashboard')?>">
                            <a href="<?php echo site_url('admin/dashboard'); ?>">
                                <i class="fa fa-dashboard"></i> <span><?php echo lang('menu_dashboard'); ?> Ticket</span>
                            </a>
                        </li>-->
                        

                       
                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="#">
                                <i class="fa fa-plus-square"></i>
                                <span>Finance Dedicated  </span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class=""><a href="<?php echo site_url('admin/invoice/proforma'); ?>">Proforma Invoice</a></li>
                                <li class=""><a href="<?php echo site_url('/admin/invoice'); ?>">Progress Invoice</a></li>
                                <li class=""><a href="<?php echo site_url('/admin/invoice/progres_bulk'); ?>">Progress Bulk</a></li>
                            </ul>
                        </li>
                      <!--   <li class="">
                            <a href="<?php echo site_url('admin/invoice/proforma'); ?>">
                                <i class="fa  fa-plus-square"></i> <span>Proforma Invoice</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo site_url('/admin/invoice'); ?>">
                                <i class="fa fa-print"></i> <span>Progress Invoice</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo site_url('/admin/invoice/progres_bulk'); ?>">
                                <i class="fa fa-print"></i> <span>Progress Bulk</span>
                            </a>
                        </li>
 -->
                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="#">
                                <i class="fa fa-plus-square-o"></i>
                                <span>Finance Retail </span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class=""><a href="<?php echo site_url('admin/invoice/proforma_retail'); ?>">Proforma Invoice</a></li>
                                <li class=""><a href="<?php echo site_url('/admin/invoice/retail'); ?>">Progress Invoice</a></li>
                                <li class=""><a href="<?php echo site_url('/admin/invoice/progres_bulk_retail'); ?>">Progress Bulk</a></li>
                            </ul>
                        </li>
                        <!-- <li class="header text-uppercase">Finance Retail</li>
                        <li class="">
                            <a href="<?php echo site_url('admin/invoice/proforma_retail'); ?>">
                                <i class="fa fa-plus-square-o"></i> <span>Proforma Invoice</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo site_url('/admin/invoice/retail'); ?>">
                                <i class="fa fa-file-text"></i> <span>Progress Invoice</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo site_url('/admin/invoice/progres_bulk_retail'); ?>">
                                <i class="fa fa-print"></i> <span>Progress Bulk</span>
                            </a>
                        </li> -->
                       <!--  <li class="header text-uppercase">Bulk</li> -->
                       <li class="">
                            <a href="<?php echo site_url('/admin/invoice/invoice_close'); ?>">
                                <i class="fa fa-file-text"></i> <span>Invoice Closed</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?php echo site_url('/admin/invoice/bulk'); ?>">
                                <i class="fa fa-file-text"></i> <span>Bulk Invoice</span>
                            </a>
                        </li>

                      <!--   <li class="header text-uppercase">Summary & Log</li> -->
                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                <span>Log</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class=""><a href="<?php echo site_url('admin/invoice/summary'); ?>">Summary Invoice</a></li>
                                <li class=""><a href="<?php echo site_url('admin/invoice/invoice_log'); ?>">Log Invoice</a></li>
                                <li class=""><a href="<?php echo site_url('admin/invoice/proforma_log'); ?>">Log Proforma</a></li>
                                <li class=""><a href="<?php echo site_url('admin/customer/log_matikan'); ?>">Log Customer ON OFF </a></li>
                                <li class=""><a href="<?php echo site_url('admin/customer/log_whatsapp'); ?>">Log Whatsapp </a></li>
                            </ul>
                        </li>
                       <!--  <li class="header text-uppercase">Power On & OFF </li> -->
                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Customer</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class=""><a href="<?php echo site_url('admin/customer/registasi'); ?>">Registasi customer </a></li>
                                <li class=""><a href="<?php echo site_url('admin/customer'); ?>">List Customer</a></li>
                                <li class=""><a href="<?php echo site_url(''); ?>">Dismentel </a></li>
                               
                            </ul>
                        </li>
                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                <span>Mitra</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class=""><a href="<?php echo site_url('admin/mitra'); ?>">Mitra</a></li>
                                <li class=""><a href="<?php echo site_url('admin/cashback'); ?>">Cashback</a></li>
                               
                            </ul>
                        </li>
                        <li class=""><a href="<?php echo site_url('/admin/invoice/wa'); ?>"><i class="fa fa-bullseye"></i><span> WhatsApp </span> </a></li>
                        <!-- <li class=""><a href="<?php echo site_url('/admin/mitra'); ?>"><i class="fa fa-users"></i><span> Mitra </span> </a></li>
                        <li class=""><a href="<?php echo site_url('/admin/cashback'); ?>"><i class="fa fa-users"></i><span> Cashback </span> </a></li> -->


                        <!-- <li class="header text-uppercase"><?php echo lang('menu_administration'); ?></li>
                        <li class="<?=active_link_controller('users')?>">
                            <a href="<?php echo site_url('admin/users'); ?>">
                                <i class="fa fa-user"></i> <span><?php echo lang('menu_users'); ?></span>
                            </a>
                        </li>
                        <li class="<?=active_link_controller('groups')?>">
                            <a href="<?php echo site_url('admin/groups'); ?>">
                                <i class="fa fa-shield"></i> <span><?php echo lang('menu_security_groups'); ?></span>
                            </a>
                        </li>
                        <li class="treeview <?=active_link_controller('prefs')?>">
                            <a href="#">
                                <i class="fa fa-cogs"></i>
                                <span><?php echo lang('menu_preferences'); ?></span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?=active_link_function('interfaces')?>"><a href="<?php echo site_url('admin/prefs/interfaces/admin'); ?>"><?php echo lang('menu_interfaces'); ?></a></li>
                            </ul>
                        </li> --> 
                        <!--<li class="<?=active_link_controller('files')?>">
                            <a href="<?php echo site_url('admin/files'); ?>">
                                <i class="fa fa-file"></i> <span><?php echo lang('menu_files'); ?></span>
                            </a>
                        </li>
                        <li class="<?=active_link_controller('database')?>">
                            <a href="<?php echo site_url('admin/database'); ?>">
                                <i class="fa fa-database"></i> <span><?php echo lang('menu_database_utility'); ?></span>
                            </a>
                        </li>


                        <li class="header text-uppercase"><?php echo $title; ?></li>
                        <li class="<?=active_link_controller('license')?>">
                            <a href="<?php echo site_url('admin/license'); ?>">
                                <i class="fa fa-legal"></i> <span><?php echo lang('menu_license'); ?></span>
                            </a>
                        </li>
                        <li class="<?=active_link_controller('resources')?>">
                            <a href="<?php echo site_url('admin/resources'); ?>">
                                <i class="fa fa-cubes"></i> <span><?php echo lang('menu_resources'); ?></span>
                            </a>
                        </li>-->
                    </ul>
                </section>
            </aside>
