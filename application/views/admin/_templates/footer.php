<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b><?php echo lang('footer_version'); ?></b> 1.3.0
                </div>
                <strong><?php echo lang('footer_copyright'); ?> &copy; 2018-<?php echo date('Y'); ?> <a href="http://gooptix.id" target="_blank">Gooptix</a> &amp; <a href="#" target="_blank">Firman Syahruman</a>.</strong> <?php echo lang('footer_all_rights_reserved'); ?>.
            </footer>
        </div>

        <script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/jquery/jquery.cookie.js'); ?>"></script>
        <script src="http://www.chartjs.org/dist/2.7.2/Chart.bundle.js"></script>
        <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        
        <script src="<?php echo base_url($frameworks_dir . '/jquery/scripts.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/datatables.net-bs/js/dataTables.bootstrap.min.jss'); ?>"></script>
         
        <script src="<?php echo base_url($frameworks_dir . '/bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url($plugins_dir . '/slimscroll/slimscroll.min.js'); ?>"></script>

        <script src="<?php echo base_url($frameworks_dir. '/tableExport/xlsx.core.js')?>"></script>
        <script src="<?php echo base_url($frameworks_dir.'/tableExport/Blob.js')?>"></script>
        <script src="<?php echo base_url($frameworks_dir.'/tableExport/FileSaver.js')?>"></script>
        <script src="<?php echo base_url($frameworks_dir.'/tableExport/tableexport.js')?>"></script>


<?php if ($mobile == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/fastclick/fastclick.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($admin_prefs['transition_page'] == TRUE): ?>
        <script src="<?php echo base_url($plugins_dir . '/animsition/animsition.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'users' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/pwstrength/pwstrength.min.js'); ?>"></script>
<?php endif; ?>
<?php if ($this->router->fetch_class() == 'groups' && ($this->router->fetch_method() == 'create' OR $this->router->fetch_method() == 'edit')): ?>
        <script src="<?php echo base_url($plugins_dir . '/tinycolor/tinycolor.min.js'); ?>"></script>
        <script src="<?php echo base_url($plugins_dir . '/colorpickersliders/colorpickersliders.min.js'); ?>"></script>
<?php endif; ?>
        <script src="<?php echo base_url($frameworks_dir . '/adminlte/js/adminlte.min.js'); ?>"></script>
        <script src="<?php echo base_url($frameworks_dir . '/domprojects/js/dp.min.js'); ?>"></script>
        <script type="text/javascript">
            var xport = {
              _fallbacktoCSV: true,  
              toXLS: function(tableId, filename) {   
                this._filename = (typeof filename == 'undefined') ? tableId : filename;
                
                //var ieVersion = this._getMsieVersion();
                //Fallback to CSV for IE & Edge
                if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
                  return this.toCSV(tableId);
                } else if (this._getMsieVersion() || this._isFirefox()) {
                  alert("Not supported browser");
                }

                //Other Browser can download xls
                var htmltable = document.getElementById(tableId);
                var html = htmltable.outerHTML;

                this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls'); 
              },
              toCSV: function(tableId, filename) {
                this._filename = (typeof filename === 'undefined') ? tableId : filename;
                // Generate our CSV string from out HTML Table
                var csv = this._tableToCSV(document.getElementById(tableId));
                // Create a CSV Blob
                var blob = new Blob([csv], { type: "text/csv" });

                // Determine which approach to take for the download
                if (navigator.msSaveOrOpenBlob) {
                  // Works for Internet Explorer and Microsoft Edge
                  navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
                } else {      
                  this._downloadAnchor(URL.createObjectURL(blob), 'csv');      
                }
              },
              _getMsieVersion: function() {
                var ua = window.navigator.userAgent;

                var msie = ua.indexOf("MSIE ");
                if (msie > 0) {
                  // IE 10 or older => return version number
                  return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
                }

                var trident = ua.indexOf("Trident/");
                if (trident > 0) {
                  // IE 11 => return version number
                  var rv = ua.indexOf("rv:");
                  return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
                }

                var edge = ua.indexOf("Edge/");
                if (edge > 0) {
                  // Edge (IE 12+) => return version number
                  return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
                }

                // other browser
                return false;
              },
              _isFirefox: function(){
                if (navigator.userAgent.indexOf("Firefox") > 0) {
                  return 1;
                }
                
                return 0;
              },
              _downloadAnchor: function(content, ext) {
                  var anchor = document.createElement("a");
                  anchor.style = "display:none !important";
                  anchor.id = "downloadanchor";
                  document.body.appendChild(anchor);

                  // If the [download] attribute is supported, try to use it
                  
                  if ("download" in anchor) {
                    anchor.download = this._filename + "." + ext;
                  }
                  anchor.href = content;
                  anchor.click();
                  anchor.remove();
              },
              _tableToCSV: function(table) {
                // We'll be co-opting `slice` to create arrays
                var slice = Array.prototype.slice;

                return slice
                  .call(table.rows)
                  .map(function(row) {
                    return slice
                      .call(row.cells)
                      .map(function(cell) {
                        return '"t"'.replace("t", cell.textContent);
                      })
                      .join(",");
                  })
                  .join("\r\n");
              }
            };
        </script>

    </body>
</html>