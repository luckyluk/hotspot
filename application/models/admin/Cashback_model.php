<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cashback_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('spc', TRUE);
        $this->db3 = $this->load->database('master', TRUE);
        $this->db5 = $this->load->database();
    }


    public function get_Cashback_all($mitraid,$custid,$periode)
    {  

        $this->db->select('`spc`.`cashback`.`no`,`gooptix_master`.`customer_activation`.`Customer_Sub_Name` as nama_customer,`gooptix_master`.`mitra`.`name` as nama_mitra,`spc`.`cashback`.`cashback`,`spc`.`cashback`.`periode`,`spc`.`cashback`.`status`,`spc`.`cashback`.`created_date`,`spc`.`cashback`.`created_by`');
        $this->db->join('gooptix_master`.`customer_activation`','`spc`.`cashback`.`id_customer` = `gooptix_master`.`customer_activation`.`Task_ID`', 'left');
        $this->db->join('`gooptix_master`.`mitra`', '`spc`.`cashback`.`id_mitra` = `gooptix_master`.`mitra`.`id`', 'left');
        if($mitraid !=""){
            $this->db->where('`spc`.`cashback`.`id_mitra`',$mitraid);
        }
        if($custid !=""){
            $this->db->where('`spc`.`cashback`.`id_customer`',$custid);
        }
        if($periode !=""){
            $this->db->like('`spc`.`cashback`.`periode`',$periode);
        }
        $this->db->where('`spc`.`cashback`.`cashback` !=','0');
        $query = $this->db->get('cashback')->result();
        return $query;
    }

    function get_list_customer(){

        $this->db->select('`spc`.`cashback`.`id_customer`,`gooptix_master`.`customer_activation`.`Customer_Sub_Name` as nama_customer');
        $this->db->join('gooptix_master`.`customer_activation`','`spc`.`cashback`.`id_customer` = `gooptix_master`.`customer_activation`.`Task_ID`', 'left');
        //$this->db->join('`gooptix_master`.`mitra`', '`spc`.`cashback`.`id_mitra` = `gooptix_master`.`mitra`.`id`', 'left');

        $this->db->distinct();
        $query = $this->db->get('cashback')->result();
        return $query;

    }

    function get_list_mitra(){

        $this->db3->select('*');
        $query = $this->db3->get('mitra')->result();
        return $query;

    }
    public function update_cashback($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

}
