<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daerah_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('daerah', TRUE);
        //$this->db3 = $this->load->database('master', TRUE);
        //$this->db5 = $this->load->database();
    }


    public function get_province_all()
    {     
        $query = $this->db->query("SELECT nama FROM `provinsi`");

        return $query->result();
    }

    public function get_city()
    {     
        $query = $this->db->query("SELECT * FROM `kabupaten`");

        return $query->result();
    }

    public function get_city_id($id)
    {     
        $query = $this->db->query("SELECT * FROM `kabupaten` where id_prov like $id");

        return $query->result();
    }

    

    


    


    
}
