<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('spc', TRUE);
        $this->db3 = $this->load->database('master', TRUE);
        $this->db5 = $this->load->database();
    }


    public function get_proforma_all()
    {     
        $query = $this->db->query("SELECT * FROM `proforma` where `Status` LIKE 'CREATED'");

        return $query->result();
    }

    public function get_proforma_retail()
    {     
        $query = $this->db->query("SELECT * FROM `proforma` where `Status` LIKE 'CREATED' and `Sub_Product` not IN('CORPORATE','DEDICATED') order by `City` ASC");

        return $query->result();
    }

    public function get_proforma_retail_city($city)
    {     
        $strcity = (string) $city;
        $query = $this->db->query("SELECT * FROM `proforma` where City like '$city' and `Status` LIKE 'CREATED' and `Sub_Product` not IN('CORPORATE','DEDICATED')TE' order by `City` ASC");

        return $query->result();
    }

    public function get_proforma_dedicated()
    {     
        //$query = $this->db->query("SELECT * FROM `proforma` where `Status` LIKE 'CREATED' and `Sub_Product` not like 'RETAIL' order by (`Invoice_No` * 1)");
        $query = $this->db->query("SELECT * FROM `proforma` where `Status` LIKE 'CREATED' and `Sub_Product` IN('CORPORATE','DEDICATED') order by `City` ASC");

        return $query->result();
    }

    public function get_proforma_by_no($noinv)
    {     
        $query = $this->db->query("SELECT * FROM `proforma` where `Invoice_No` like '$noinv' and `Status` like 'CREATED'");

        return $query->result();
    }



    public function update_proforma($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }   

    public function create_invoice($data){
        $this->db->insert('invoice',$data);
    }

    public function create_proforma($data){
        $this->db->insert('proforma',$data);
    }

    public function update_proforma_status($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    public function insert_proforma_log($datalog){
        $this->db->insert('proforma_log',$datalog);
    }

    public function get_invoice_all()
    {     
        $query = $this->db->query("SELECT * FROM `invoice` where `Status` like 'CREATED'");

        return $query->result();
    }

    public function get_invoice_dedicated($city,$invdate,$name)
    {   
        $strcity = (string) $city;  
        $query = $this->db->query("SELECT * FROM `invoice`  where  `City` like '%$strcity%' and `Inv_Date` like '%$invdate%' and `Account_Name` like '%$name%' and `Sub_Product` IN('CORPORATE','DEDICATED') and `Status` NOT like 'CLOSED' and 'Invoice_Type' NOT like 'BULK' order by 'Start_Billing' asc");

        return $query->result();
    }

    public function get_invoice_dedicated_closed($city,$invdate,$name)
    {   
        $strcity = (string) $city;  
        $query = $this->db->query("SELECT * FROM `invoice`  where  `City` like '%$strcity%' and `Inv_Date` like '%$invdate%' and `Account_Name` like '%$name%' and `Sub_Product` IN('CORPORATE','DEDICATED') and `Status` like 'CLOSED' and 'Invoice_Type' NOT like 'BULK' order by 'Start_Billing' asc");

        return $query->result();
    }
    public function get_invoice_dedicated_bulk($city,$invdate,$name)
    {   
        $strcity = (string) $city;  
        $query = $this->db->query("SELECT * FROM `invoice`  where  `City` like '%$strcity%' and `Inv_Date` like '%$invdate%' and `Account_Name` like '%$name%' and `Sub_Product` IN('CORPORATE','DEDICATED') and `Status` NOT like 'CLOSED' and 'Invoice_Type'  like 'BULK'order by 'Start_Billing' asc");

        return $query->result();
    }
    public function get_invoice_retail_bulk($city,$invdate,$name)
    {   
        $strcity = (string) $city;  
        $query = $this->db->query("SELECT * FROM `invoice`  where  `City` like '%$strcity%' and `Inv_Date` like '%$invdate%' and `Account_Name` like '%$name%' and `Sub_Product`  not IN('CORPORATE','DEDICATED') and `Status` NOT like 'CLOSED' and 'Invoice_Type'  like 'BULK' order by 'Start_Billing' asc");

        return $query->result();
    }

    /*public function get_invoice_dedicated_city($city)
    {     
        $strcity = (string) $city;
        $query = $this->db->query("SELECT * FROM `invoice` where  `City` like '$strcity' and `Sub_Product` not like 'RETAIL' and `Status` NOT like 'CLOSED'");

        return $query->result();
    }*/

    /*public function get_invoice_retail()
    {     
        $query = $this->db->query("SELECT * FROM `invoice` where `Sub_Product` like 'RETAIL' and `Status` NOT like 'CLOSED'");
        

        return $query->result();
    }*/

    public function get_invoice_retail($city,$invdate,$name)
    {     
        $strcity = (string) $city;
        $query = $this->db->query("SELECT * FROM `invoice` where  `City` like '%$strcity%' and `Inv_Date` like '%$invdate%' and `Account_Name` like '%$name%' and `Sub_Product` not IN('CORPORATE','DEDICATED') and `Status` NOT like 'CLOSED' order by 'Start_Billing' asc");

        return $query->result();
    }

    public function get_invoice_closed_retail($city,$invdate,$name)
    {     
        $strcity = (string) $city;
        $query = $this->db->query("SELECT * FROM `invoice` where  `City` like '%$strcity%' and `Inv_Date` like '%$invdate%' and `Account_Name` like '%$name%' and `Sub_Product` not IN('CORPORATE','DEDICATED') and `Status`  like 'CLOSED' order by 'Start_Billing' asc");

        return $query->result();
    }


    public function get_invoice_no($inv_no)
    {     
        $query = $this->db->query("SELECT * FROM `invoice` where `Inv_No` like '$inv_no'");

        return $query->result();
    }

    public function insert_invoice_log($datalog){
        $this->db->insert('invoice_log',$datalog);
    }

    public function update_invoice_status($where,$data,$table){
        $this->db->where($where);
        $this->db->update('invoice',$data);
    } 

    public function get_invoice_filter($where)
    {     
        //$this->db->where($where);
        $query = $this->db->where($where)->get('invoice')->result();
        return $query;
    }

    public function get_account_name(){
        $this->db->distinct();
        $this->db->select('Account_Name,Task_ID');
        $this->db->order_by("Account_Name", "asc");
        $query = $this->db->get('invoice')->result();
        return $query;
    }

    public function get_sub_product(){
        $this->db->distinct();
        $this->db->select('Sub_Product');
        $query = $this->db->get('invoice')->result();
        return $query;
    }

    public function get_invoice_date(){
        $this->db->distinct();
        $this->db->select('Inv_Date');
        $this->db->order_by("Inv_Date", "desc");
        $query = $this->db->get('invoice')->result();
        return $query;
    }

    public function get_city(){
        $this->db->distinct();
        $this->db->select('City');
        $this->db->order_by("City", "asc");
        $query = $this->db->get('invoice')->result();
        return $query;
    }

    public function get_account_sub_name($where){
        $this->db->distinct();
        $this->db->select('Account_Sub_Name');
        $this->db->where($where);
        $this->db->order_by("Account_Sub_Name", "asc");
        $query = $this->db->get('invoice')->result();
        return $query;
    }



    public function get_proforma_log($where){
        $query = $this->db->like($where)->get('proforma_log')->result();
        return $query;
    }

    public function get_invoice_log($where){
        // $query = $this->db->like($where)->get('invoice_log' ORDER BY 'Dateinsert')->result();
        // return $query;
       
        $query = $this->db->query("SELECT * FROM `invoice_log` ORDER BY `Dateinsert` DESC");

        return $query->result();
    }

    public function get_invoice_created_by_month($date){
        $strdate = (string) $date;
        $query = $this->db->query("SELECT count(*) as jumlah_invoice FROM `invoice` where `Inv_Date` like '$strdate'");

        return $query->result();

    }

    public function get_invoice_closed_by_month($date){
        $strdate = (string) $date;
        $query = $this->db->query("SELECT count(*) as jumlah_invoice FROM `invoice` where `Inv_Date` like '$strdate' and `Status` like 'CLOSED'");

        return $query->result();

    }

    public function get_value_invoice_created(){
        $query = $this->db->query("SELECT inv_date, sum(Subtotal) as nilai_invoice FROM `invoice` GROUP by Inv_Date order by Inv_Date asc");

        return $query->result();

    }

    public function get_value_invoice_created_by_month($date){
        $strdate = (string) $date;
        $query = $this->db->query("SELECT sum(Subtotal) as nilai_invoice FROM `invoice` where `Inv_Date` like '$strdate'");

        return $query->result();

    }

    /*public function get_value_invoice_created($date){
        $strdate = (string) $date;
        $query = $this->db->query("SELECT sum(Subtotal) as nilai_invoice FROM `invoice` where `Inv_Date` like '$strdate'");

        return $query->result();

    }*/

    public function get_value_invoice_closed(){
        $query = $this->db->query("SELECT inv_date, sum(Subtotal) as nilai_invoice FROM `invoice` where `Status` like 'CLOSED'GROUP by Inv_Date order by Inv_Date asc");

        return $query->result();

    }

    public function get_value_invoice_closed_by_month($date){
        $strdate = (string) $date;
        $query = $this->db->query("SELECT sum(Subtotal) as nilai_invoice FROM `invoice` where `Inv_Date` like '$strdate' and `Status` like 'CLOSED'");

        return $query->result();

    }

    public function get_join(){
                $this->db->select('invoice.Account_No,gooptix_master.customer_activation.username');
                 $this->db->from('invoice');
                 //$this->db->join('gooptix_master.customer_activation', 'gooptix_master.customer_activation.Customer_ID =invoice.Account_No');
                //$this->where('base2.table3.item4','toto')
                //$query = $this->db->get();
                //echo $query;
                return $query;
                //print_r($query);
    }

    public function get_name_bulk(){
        $query = $this->db->query("select DISTINCT `Account_Name`,`Sub_Product`,`Inv_Date`,`Status` from invoice where `Invoice_Type` like 'BULK'");
        return $query->result();

    }

    public function get_detail_bulk($name,$product,$bulan,$tahun){
        $query = $this->db->query("select * from invoice where `Account_Name` like '$name' and `Sub_Product` like '$product' and Inv_Date  like '$tahun-$bulan%'");
        return $query->result();

    }
     public function insert_cashback($cashback){
        $this->db->insert('cashback',$cashback);
    }

    public function get_value_closed_in_month($date){
        $strdate = (string) $date;
        $query = $this->db->query("SELECT DATE_FORMAT(`invoice_log`.`Dateinsert`, '%Y-%m') as month,sum(`invoice`.`Subtotal`) as value FROM `invoice_log` join `invoice` on `invoice_log`.`Inv_No` = `invoice`.`Inv_No` WHERE  `invoice_log`.`Status` LIKE '%CLOSED%' GROUP BY DATE_FORMAT(`invoice_log`.`Dateinsert`, '%Y-%m')");

        return $query->result();
    }

    public function insert_log_pembayaran($databayar){
        $this->db->insert('pembayaran_log',$databayar);
    }

    public function get_detail_custom($name){
        $query = $this->db->query("select * from invoice where `Account_Sub_Name` like '$name' and Status like 'CREATED' ORDER BY `invoice`.`Inv_Date` DESC");
        return $query->result();

    }

    public function get_invoice_per_user($regid)
    {     
        $query = $this->db->query("SELECT * FROM `invoice` where `Reg_ID` like '$regid'");

        return $query->result();
    }

    public function get_detail_per_user($arrayinvoice){
        $query = $this->db->query("select * from invoice where Inv_No IN($arrayinvoice) ORDER BY `invoice`.`Inv_Date` DESC");
        return $query->result();

    }

    function insertNotices($arrayOfNoticeFiles){
        $tableName  = "invoice";
        $inputArray = $arrayOfNoticeFiles;
 
        $data = array(
            'payment_folder_file'               => $inputArray["document_foldername"],
            'payment_file'                 => $inputArray["document_filename"]
        );
 
        $this->db->insert($tableName, $data); 
    }

        public function get_whatsapp()
    {     
        $query = $this->db->query("SELECT * FROM `whatsapp`");

        return $query->result();
    }

    




    


    


    
}
