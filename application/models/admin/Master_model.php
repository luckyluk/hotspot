<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->db3 = $this->load->database('master', TRUE);
        $this->db = $this->load->database('spc', TRUE);
    }


    public function get_activation_all()
    {     
        //$query = $this->db3->query("SELECT * FROM `customer_activation` WHERE `Status` NOT IN('ACTIVE','CANCELED') AND Registration_Date Not like '0000-00-00' ORDER BY `Registration_Date` DESC");
        $query = $this->db3->query("SELECT * FROM `customer_activation` ORDER BY `Registration_Date` DESC");

        return $query->result();
    }

    public function get_customer_status($city,$bulan,$status){
        //$query = $this->db3->query("SELECT customer_activation.Reg_ID ,customer_activation.City,customer_activation.Customer_Name , customer_activation.Customer_Sub_Name ,matikan.status FROM `customer_activation` left join `matikan` on customer_activation.Reg_ID = matikan.Reg_ID left join `outstanding` on customer_activation.Reg_ID = outstanding.Reg_ID where customer_activation.Status LIKE 'ACTIVE' and customer_activation.City like '%$city%'");
        //return $query->result();

        $this->db3->select('customer_activation.Reg_ID ,customer_activation.City,customer_activation.Customer_Name , customer_activation.Customer_Sub_Name ,matikan.status');
        $this->db3->from('customer_activation');
        $this->db3->join('matikan', 'customer_activation.Reg_ID = matikan.Reg_ID', 'left');
        $this->db3->join('outstanding', 'customer_activation.Reg_ID = outstanding.Reg_ID', 'left'); 
        $this->db3->where('customer_activation.Status','ACTIVE');
        if($city != ""){
            $this->db3->where('customer_activation.City',$city);
        }
        if($status == '2'){
            $this->db3->where('matikan.status','2');
        }
        $query = $this->db3->get();
        return $query->result();
    }


    public function get_customer_regid($regid){
        $query = $this->db3->query(" SELECT * FROM `customer_activation` left join `mitra` on `customer_activation`.`id_mitra`=`mitra`.`id` where `Reg_ID` like '$regid'");
        //$query = $this->db3->query(" SELECT * FROM `customer_activation` where `Reg_ID` like '$regid'");
        return $query->result();
    }

    public function offlink($data){
        $this->db3->insert('matikan',$data);

    // It may be that $deal_id wasn't found, 
    // but we can check for an error, anyway.
    $error = $this->db3->error();

    // If an error occurred, $error will now have 'code' and 'message' keys...
        if (isset($error['message'])) {
            return $error['message'];
        }

    // No error returned by the DB driver... 
    return null;
    }

    public function offlink_log($datalog){
        $this->db3->insert('matikan_log',$datalog);
        $error = $this->db3->error();
        if (isset($error['message'])) {
            return $error['message'];
        }
        return null;
    }

    public function onlink($data){
        $this->db3->delete('matikan',$data);

    // It may be that $deal_id wasn't found, 
    // but we can check for an error, anyway.
    $error = $this->db3->error();

    // If an error occurred, $error will now have 'code' and 'message' keys...
    if (isset($error['message'])) {
        return $error['message'];
    }

    // No error returned by the DB driver... 
    return null;
    }

    public function onlink_log($datalog){
        $this->db3->insert('matikan_log',$datalog);
        $error = $this->db3->error();
        if (isset($error['message'])) {
            return $error['message'];
        }
        return null;
    }

    public function get_outstanding_regid($regid){
        $query = $this->db3->query(" SELECT `1` as jan, `2` as feb, `3` as mar, `4` as apr, `5` as mei, `6` as jun, `7` as jul, `8` as agu, `9` as sep, `10` as okt, `11` as nov, `12` as des from `outstanding` WHERE `Reg_ID` LIKE $regid");
        return $query->result();
    }

    public function get_outstanding_2018_regid($regid){
        $query = $this->db3->query(" SELECT `1` as jan, `2` as feb, `3` as mar, `4` as apr, `5` as mei, `6` as jun, `7` as jul, `8` as agu, `9` as sep, `10` as okt, `11` as nov, `12` as des from `outstanding_2018` WHERE `Reg_ID` LIKE $regid");
        return $query->result();
    }

    public function update_outstanding($regid,$bulan,$table){
        $intbulan = (int)$bulan;
        $query = $this->db3->query("update $table set `$intbulan`=1 where `reg_id` like $regid");
        //$this->db->where($where);
        //$this->db->update($table,$data);
        return $query;
    } 

    public function get_regid($urut){

        $query = $this->db3->query("SELECT Reg_ID FROM `customer_activation` where Task_ID like '$urut'");
        return $query->result();

    }
    public function get_invoice_regid($regid){
        $query = $this->db->query("SELECT `invoice`.`Inv_No`,`invoice`.`Inv_Date`,`invoice`.`Total`,`invoice`.`Created_By`,`invoice_log`.`Dateinsert`,`invoice`.`Status` from `invoice`  join `invoice_log` on `invoice`.`Inv_No` = `invoice_log`.`Inv_No` where `invoice_log`.`Status` like 'Invoice CLOSED' and `invoice`.`reg_id` LIKE '$regid' order by `invoice`.`Inv_Date` DESC");
        return $query->result();
    }

    public function get_cashback_regid($regid){
        $query = $this->db->query("SELECT * FROM `cashback` WHERE `id_customer` LIKE (SELECT distinct CAST(right(Inv_No,3) as UNSIGNED) as taskid FROM `invoice` where Reg_ID='$regid') order by periode DESC");
        return $query->result();
    }
    public function get_invoice_log($invno){
        $query = $this->db->query(" SELECT * from `invoice_log` WHERE `inv_no` LIKE '$invno'");
        return $query->result();
    }
    public function get_account_name(){
        $query = $this->db3->query(" SELECT distinct Customer_Name,Task_ID from customer_activation order by Customer_Name asc");
        return $query->result();
    }
    public function create_prospek($data){
        $this->db3->insert('customer_activation',$data);
    }

    public function get_last_taskid(){
        $query = $this->db3->query("SELECT Task_ID FROM `customer_activation` ORDER BY `customer_activation`.`ID` desc limit 1");
        return $query->result();
    }

    public function get_log_matikan(){
        $query = $this->db3->query("SELECT `customer_activation`.`Customer_Name`,`customer_activation`.`Customer_Sub_Name`,`matikan_log`.`Created_By`,`matikan_log`.`Dateinsert`,`matikan_log`.`Status` FROM `matikan_log` join `customer_activation` on `matikan_log`.`Reg_ID` = `customer_activation`.`Reg_ID` ORDER BY `matikan_log`.`Dateinsert` DESC");
        return $query->result();
        
    }


    public function get_log_whatsapp(){
        $query = $this->db->query("SELECT * FROM `whatsapp' ");
        return $query->result();
        
    }

    public function insert_wa($data){
        $this->db->insert('whatsapp',$data);
        $error = $this->db->error();
        if (isset($error['message'])) {
            return $error['message'];
        }
        return null;
    }
    

}


