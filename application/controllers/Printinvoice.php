<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PrintInvoice extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('number');
        $this->load->model('admin/invoice_model');
        $this->load->model('admin/master_model');
    }

	public function preview_invoice()
    {
            /* Title Page */

            /*Proforma*/

            
            $inv_no = $this->uri->segment(3);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_preview_invoice', $this->data);
    }

    public function preview_invoice_bekasi()
    {
            /* Title Page */

            /*Proforma*/

            
            $inv_no = $this->uri->segment(3);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_preview_invoice_bekasi', $this->data);
    }

    public function preview_invoice_instalasi_bekasi()
    {
            /* Title Page */

            /*Proforma*/

            
            $inv_no = $this->uri->segment(3);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_preview_invoice_instalasi_bekasi', $this->data);
    }
    public function print_invoice($no_inv)
    {

            
            $inv_no = $this->uri->segment(3);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_print_invoice', $this->data);


            /* inser invoice log*/  
            $loginname = $this->input->cookie('identity');  
            $datalog = array(   'Inv_No' => $datainv[0]->Inv_No,
                            'Created_By' => "Downloader",
                            'Status' => 'Invoice Di Print'
            );
            $this->invoice_model->insert_Invoice_log($datalog);
    }

    public function preview_invoice_instalasi()
    {
            /* Title Page */

            /*Proforma*/

            
            $inv_no = $this->uri->segment(3);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_preview_invoice_instalasi', $this->data);
    }

    public function print_invoice_dedicated($no_inv)
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            
            $inv_no = $this->uri->segment(4);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_print_invoice_dedicated', $this->data);


            /* inser invoice log*/  
            $loginname = $this->input->cookie('identity');  
            $datalog = array(   'Inv_No' => $datainv[0]->Inv_No,
                            'Created_By' => "Print User",
                            'Status' => 'Invoice Di Print'
            );
            $this->invoice_model->insert_Invoice_log($datalog);

            
        }
    }
}
