<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/master_model');
        $this->load->model('admin/invoice_model');
        $this->load->model('admin/master_model');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $city = $this->input->get('city' , TRUE);
            $bulan = $this->input->get('bulan' , TRUE);
            $status = $this->input->get('status' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            //$this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated();

            /* Load Template */
            
            $this->data['customer'] = $this->master_model->get_customer_status($city,$bulan,$status);

            $this->template->admin_render('admin/customer/v_customer', $this->data);

            
        }
	}

    public function get_customer($regid){
        $regid = $this->uri->segment(4);
        $invoicearray = $this->master_model->get_customer_regid($regid);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function offlink($regid){
        $regid = $this->uri->segment(4);
        $customerdata = $this->master_model->get_customer_regid($regid);
        $loginname = $this->ion_auth->user()->row()->username; 
        foreach ($customerdata as $key) {

            $data = array(  'username' => $key->username,
                        'Reg_ID' => $key->Reg_ID,
                        'city' => $key->City,
                        'tanggal' => '2017-01-01',
                        'status' => '2'
             );

            $datalog = array(   'Reg_ID' => $key->Reg_ID,
                                'Created_By' => $loginname,
                                'Status' => 'dimatikan'
                             );
            $this->master_model->offlink($data);
            $this->master_model->offlink_log($datalog);
            # code...
        }
        redirect('admin/customer');
    }

    public function onlink($regid){
        $regid = $this->uri->segment(4);
        $customerdata = $this->master_model->get_customer_regid($regid);
        $loginname = $this->ion_auth->user()->row()->username; 
        foreach ($customerdata as $key) {

            $data = array(
                        'Reg_ID' => $key->Reg_ID,
             );
            $datalog = array(   'Reg_ID' => $key->Reg_ID,
                                'Created_By' => $loginname,
                                'Status' => 'dinyalakan'
                             );
            $this->master_model->onlink($data);
            $this->master_model->onlink_log($datalog);
            # code...
        }
        redirect('admin/customer');
    }

    public function detail()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            //$this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated();

            /* Load Template */

            $regid = $this->uri->segment(4);
            $this->data['customer'] = $this->master_model->get_customer_regid($regid);
            $this->data['outstanding'] = $this->master_model->get_outstanding_regid($regid);
            $this->data['outstanding_2018'] = $this->master_model->get_outstanding_2018_regid($regid);
            $this->data['invoice'] = $this->master_model->get_invoice_regid($regid);
            $this->data['cashback'] = $this->master_model->get_cashback_regid($regid);
            // $this->data['invoice_log'] = $this->master_model->get_invoice_log($invno);


            //$this->data['customer'] = $this->master_model->get_customer_status();

            $this->template->admin_render('admin/customer/v_customer_detail', $this->data);

            
            

            
        }
    }

    public function addprospek(){

        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            //$this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated();

            /* Load Template */

            //$regid = $this->uri->segment(4);
            //$this->data['customer'] = $this->master_model->get_customer_regid($regid);
            //$this->data['outstanding'] = $this->master_model->get_outstanding_regid($regid);
            //$this->data['invoice'] = $this->master_model->get_invoice_regid($regid);
            // $this->data['invoice_log'] = $this->master_model->get_invoice_log($invno);


            //$this->data['customer'] = $this->master_model->get_customer_status();

            $this->template->admin_render('admin/customer/v_add_prospek', $this->data);

            
            

            
        }
        
    }

    public function insert_prospek(){
        $namapelanggan = $this->input->post('namapelanggan');
        $noktp = $this->input->post('noktp');
        $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region


             );
            
        $this->master_model->create_prospek($data);
    }

    public function update_2018()
    {
        $reg_id = $this->input->get('reg_id');
        //$bulan = (int)date('m',$this->input->post('bulan'));
        $data = (int)$this->input->get('bulan');
        $status = '1';

     
        $where = $reg_id;

        $this->master_model->update_outstanding($where,$data,'outstanding');
        //$this->template->admin_render('admin/test', $this->data);
        //redirect('admin/invoice/proforma');
    }

    public function log_matikan()
    {   
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            //$this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated();

            /* Load Template */


            $this->data['customer'] = $this->master_model->get_log_matikan();

            $this->template->admin_render('admin/customer/v_customer_log', $this->data);
        }
    }
        public function log_whatsapp()
    {   
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            //$this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated();

            /* Load Template */


            $this->data['customer'] = $this->master_model->get_log_whatsapp();

            $this->template->admin_render('admin/customer/v_whatsapps_log', $this->data);
        }


    }

    public function kirim_wa(){
        $nowa = $this->input->get('nowa');
        $pesan = $this->input->get('pesan');
        $file = $this->input->get('file');
        //$tahun = $this->input->get('tahun');
        //$bulan = $this->input->get('bulan');
        //$city = $this->input->get('city');
        $folder = $this->input->get('folder');
        /*if($city == "Bekasi"){
            $folder = 'bekasi/'.$tahun.'/'.$bulan;
        }
        else{
            $folder = $tahun.'/'.$bulan;
        }*/

        $data = array(  'no_wa' => $nowa,
                            'pesan' => $pesan,
                            'status' => '1',
                            'attach' => '1',
                            'file' => $file,
                            'folder' => $folder
             );
            
        $this->master_model->insert_wa($data);

        $this->load->view('admin/customer/v_berhasilwa');

    }
     public function registasi()
    {   
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();


            $this->template->admin_render('admin/customer/v_registasi', $this->data);
        }
    }
    
}
