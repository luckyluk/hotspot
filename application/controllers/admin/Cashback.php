<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cashback extends Admin_Controller {

	public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
         $this->load->model('admin/cashback_model');
        $this->load->model('admin/master_model');
    }

	public function index()
	{
   		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {   
            $mitraid = $this->input->get('mitraid');
            $custid = $this->input->get('custid');
            $periode = $this->input->get('periode');

            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $this->data['cashback_all'] = $this->cashback_model->get_cashback_all($mitraid,$custid,$periode);
            /* Load Template */
            $this->template->admin_render('admin/cashback/list_cashback', $this->data);
            
        }
	}

    public function update()
    {
        # code...
        $cb_no = $this->input->post('arraycb');
        $status = $this->input->post('status');
        //$cb_no = '5.7/SPC/CB/VII/2018,5.7/SPC/CB/VIII/2018';
        //$status = 'testing';
        $arraycb = explode(',', $cb_no);
        foreach ($arraycb as $key) {
            # code...
            $data = array(
                'status' => $status
            );
         
            $where = array(
                'no' => $key
            );
            $this->cashback_model->update_cashback($where,$data,'cashback');
        }
        redirect('admin/cashback');
        

        //$this->cashback_model->update_cashback($where,$data,'proforma');
    }
   

}
