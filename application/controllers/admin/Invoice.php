<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/invoice_model');
        $this->load->model('admin/master_model');

            
        $this->load->helper("URL", "DATE", "URI", "FORM");

        $this->load->library('form_validation');
        $this->load->library('upload');
    }



	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $city = $this->input->get('city' , TRUE);
            $invdate = $this->input->get('invdate' , TRUE);
            $name = $this->input->get('name' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            //if(is_null($city)){
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated($city,$invdate,$name);
            //}
            //else{
            //    $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated_city($city);
            //}

            

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice', $this->data);

            
        }
	}

    public function retail()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $city = $this->input->get('city' , TRUE);
            $invdate = $this->input->get('invdate' , TRUE);
            $name = $this->input->get('name' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            //if(is_null($city)){
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_retail($city,$invdate,$name);
            //}
            //else{
             //   $this->data['invoice_all'] = $this->invoice_model->get_invoice_retail_city($city);
            //}

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice_retail', $this->data);

            
        }
    }

    public function closed()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $city = $this->input->get('city' , TRUE);
            $invdate = $this->input->get('invdate' , TRUE);
            $name = $this->input->get('name' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            //if(is_null($city)){
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated_closed($city,$invdate,$name);
            //}
            //else{
            //    $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated_city($city);
            //}

            

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice', $this->data);

            
        }
    }

    public function closedretail()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $city = $this->input->get('city' , TRUE);
            $invdate = $this->input->get('invdate' , TRUE);
            $name = $this->input->get('name' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            //if(is_null($city)){
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_closed_retail($city,$invdate,$name);
            //}
            //else{
             //   $this->data['invoice_all'] = $this->invoice_model->get_invoice_retail_city($city);
            //}

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice_retail', $this->data);

            
        }
    }

    public function proforma_retail()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {

            $city = $this->input->get('city' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            if(is_null($city)){
                $this->data['proforma_all'] = $this->invoice_model->get_proforma_retail();
            }
            else{
                $this->data['proforma_all'] = $this->invoice_model->get_proforma_retail_city($city);
            }
            

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_proforma_retail', $this->data);

            
        }
    }

    public function proforma()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            $this->data['proforma_all'] = $this->invoice_model->get_proforma_dedicated();

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_proforma', $this->data);

            
        }
    }

    public function update()
    {
        $inv_no = $this->input->post('no_inv');
        $tagihan = $this->input->post('tagihan');
        $restitusi = $this->input->post('restitusi');
        $diskon = $this->input->post('diskon');
        $total = $tagihan-$restitusi-$diskon;
        $ppn = $total*0.1;
        $totalppn = $total+$ppn;
        
        $data = array(
            'Subtotal' => $tagihan,
            'Total' => $totalppn,
            'Restitusi' => $restitusi,
            'Diskon' => $diskon,
            'PPN' => $ppn
        );
     
        $where = array(
            'Invoice_No' => $inv_no
        );

        $this->invoice_model->update_proforma($where,$data,'proforma');
        //$this->template->admin_render('admin/test', $this->data);
        redirect('admin/invoice/proforma');
    }

    public function update_retail()
    {
        $inv_no = $this->input->post('no_inv');
        $tagihan = $this->input->post('tagihan');
        $restitusi = $this->input->post('restitusi');
        $diskon = $this->input->post('diskon');
        $total = $tagihan-$restitusi-$diskon;
        $ppn = $total*0.1;
        $totalppn = $total+$ppn;
        
        $data = array(
            'Subtotal' => $tagihan,
            'Total' => $totalppn,
            'Restitusi' => $restitusi,
            'Diskon' => $diskon,
            'PPN' => $ppn
        );
     
        $where = array(
            'Invoice_No' => $inv_no
        );

        $this->invoice_model->update_proforma($where,$data,'proforma');
        //$this->template->admin_render('admin/test', $this->data);
        redirect('admin/invoice/proforma_retail');
    }

    public function update_invoice()
    {
        $regid = $this->input->post('custid');
        //$bulan = (int)date('m',$this->input->post('bulan'));
        $date = $this->input->post('bulan');
        $expdate = explode("-", $date);
        $bulan = (int)$expdate[1];
        $tahun = (int)$expdate[0];
        $tanggalbayar = $this->input->post('tanggalbayar');

        $inv_no = $this->input->post('no_inv' , TRUE);
        $status = $this->input->post('statusinv' , TRUE);

        $config['upload_path']          = 'assets/upload/bukti_bayar';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;
 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
 
        if ( ! $this->upload->do_upload('berkas')){
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('v_upload', $error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            //$this->load->view('v_upload_sukses', $data);
        }

        $upload_data = $this->upload->data();
        $file_name = $inv_no;
        $data = array(
            'Status' => $status,
            'payment_date' => $tanggalbayar,
            'payment_file' => $file_name
        );
        $loginname = $this->ion_auth->user()->row()->username;   
            $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice '.$status

            );
        $this->invoice_model->update_invoice_status($where,$data,'invoice');
        $this->invoice_model->insert_Invoice_log($datalog);

        $datacashback = $this->invoice_model->get_invoice_no($inv_no); 
               
        foreach ($datacashback as $key) {
            # code...
            $periode = $key->Inv_Date;
            $bulan = substr($periode, 5,2);
                if ($bulan == "01") {
                              # code...
                              $bulanromawi = "I";
                            }
                            elseif ($bulan == "02") {
                              # code...
                              $bulanromawi = "II";
                            }
                            elseif ($bulan == "03") {
                              # code...
                              $bulanromawi = "III";
                            }
                            elseif ($bulan == "04") {
                              # code...
                              $bulanromawi = "IV";
                            }
                            elseif ($bulan == "05") {
                              # code...
                              $bulanromawi = "V";
                            }
                            elseif ($bulan == "06") {
                              # code...
                              $bulanromawi = "VI";
                            }
                            elseif ($bulan == "07") {
                              # code...
                              $bulanromawi = "VII";
                            }
                            elseif ($bulan == "08") {
                              # code...
                              $bulanromawi = "VIII";
                            }
                            elseif ($bulan == "09") {
                              # code...
                              $bulanromawi = "IX";
                            }
                            elseif ($bulan == "X") {
                              # code...
                              $bulanromawi = "X";
                            }
                            elseif ($bulan == "11") {
                              # code...
                              $bulanromawi = "XI";
                            }
                            elseif ($bulan == "12") {
                              # code...
                              $bulanromawi = "XII";
                            }
            $noreg = substr($key->Inv_No,-4);
            $nomitra = $key->id_mitra.".".$noreg."/SPC/CB/".$bulanromawi."/".substr($periode, 0,4);
            $cashback = array(

                            'no' => $nomitra,
                            'id_mitra' => $key->id_mitra,
                            'id_customer' => (int)$noreg,
                            'cashback' => $key->cashback,
                            'periode' => $key->Inv_Date,
                            'created_by' => $loginname,
                            //'created_date' => $created_date,
                            'Status' => 'Created'
            );

            $this->invoice_model->insert_cashback($cashback);
        }


        


        
        

        if($status == "CLOSED"){
            if($tahun == "2018"){
                $this->master_model->update_outstanding($regid,$bulan,'outstanding_2018');  
            }
            else{
                $this->master_model->update_outstanding($regid,$bulan,'outstanding'); 
            }
        }
        else{

        }

        redirect('admin/invoice');
    }

    public function update_invoice_retail()
    {
        //$inv_no = $this->uri->segment(4);
        //$status = $this->uri->segment(5);

        $regid = $this->input->post('custid');
        //$bulan = (int)date('m',$this->input->post('bulan'));
        $date = $this->input->post('bulan');
        $expdate = explode("-", $date);
        $bulan = (int)$expdate[1];
        $tahun = (int)$expdate[0];
        $tanggalbayar = $this->input->post('tanggalbayar');

        $inv_no = $this->input->post('no_inv' , TRUE);
        $status = $this->input->post('statusinv' , TRUE);

        $config['upload_path']          = 'assets/upload/bukti_bayar';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;
 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
 
        if ( ! $this->upload->do_upload('berkas')){
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('v_upload', $error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            //$this->load->view('v_upload_sukses', $data);
        }

        $upload_data = $this->upload->data();
        $file = $upload_data['file_name'];
        $file_name = str_replace(" ","_","$file");
        $data = array(
            'Status' => $status,
            'payment_date' => $tanggalbayar,
            'payment_file' => $file_name
        );
     
        $where = array(
            'Inv_No' => $inv_no
        );
        $loginname = $this->ion_auth->user()->row()->username;   
            $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice '.$status

            );
        $this->invoice_model->update_invoice_status($where,$data,'invoice');
        $this->invoice_model->insert_Invoice_log($datalog);

        $datacashback = $this->invoice_model->get_invoice_no($inv_no);

        foreach ($datacashback as $key) {
            # code...
            $periode = $key->Inv_Date;
            $bulan = substr($periode, 5,2);
                if ($bulan == "01") {
                              # code...
                              $bulanromawi = "I";
                            }
                            elseif ($bulan == "02") {
                              # code...
                              $bulanromawi = "II";
                            }
                            elseif ($bulan == "03") {
                              # code...
                              $bulanromawi = "III";
                            }
                            elseif ($bulan == "04") {
                              # code...
                              $bulanromawi = "IV";
                            }
                            elseif ($bulan == "05") {
                              # code...
                              $bulanromawi = "V";
                            }
                            elseif ($bulan == "06") {
                              # code...
                              $bulanromawi = "VI";
                            }
                            elseif ($bulan == "07") {
                              # code...
                              $bulanromawi = "VII";
                            }
                            elseif ($bulan == "08") {
                              # code...
                              $bulanromawi = "VIII";
                            }
                            elseif ($bulan == "09") {
                              # code...
                              $bulanromawi = "IX";
                            }
                            elseif ($bulan == "X") {
                              # code...
                              $bulanromawi = "X";
                            }
                            elseif ($bulan == "11") {
                              # code...
                              $bulanromawi = "XI";
                            }
                            elseif ($bulan == "12") {
                              # code...
                              $bulanromawi = "XII";
                            }
            $noreg = substr($key->Inv_No,-4);
            $nomitra = $key->id_mitra.".".$noreg."/SPC/CB/".$bulanromawi."/".substr($periode, 0,4);
            $cashback = array(

                            'no' => $nomitra,
                            'id_mitra' => $key->id_mitra,
                            'id_customer' => (int)$noreg,
                            'cashback' => $key->cashback,
                            'periode' => $key->Inv_Date,
                            'created_by' => $loginname,
                            //'created_date' => $created_date,
                            'Status' => 'Created'
            );

            $this->invoice_model->insert_cashback($cashback);
        }

        
        
        

        if($status == "CLOSED"){
            if($tahun == "2018"){
                $this->master_model->update_outstanding($regid,$bulan,'outstanding_2018');  
            }
            else{
                $this->master_model->update_outstanding($regid,$bulan,'outstanding'); 
            }
        }
        else{

        }
        
        //$this->template->admin_render('admin/test', $this->data);

        
        redirect('admin/invoice/retail');
    }


    public function create()
    {
            /*
            Insert To Invoice Table
            */
        $proformainv = $this->invoice_model->get_proforma_dedicated();
        $this->data['proforma_all'] = $proformainv;
        foreach ($proformainv as $key ) {
            # code...
            if($key->Sub_Product == "RETAIL"){
                $invtype = "EMAIL";
            }
            else{
                $invtype = "DELIVERY";
            }

            //nomor faktur
                    $invno = $key->Invoice_No;
                    $periode = $key->Periode;
                    $bulan = substr($periode, 5,2);
                    if ($bulan == "01") {
                      # code...
                      $bulanromawi = "I";
                    }
                    elseif ($bulan == "02") {
                      # code...
                      $bulanromawi = "II";
                    }
                    elseif ($bulan == "03") {
                      # code...
                      $bulanromawi = "III";
                    }
                    elseif ($bulan == "04") {
                      # code...
                      $bulanromawi = "IV";
                    }
                    elseif ($bulan == "05") {
                      # code...
                      $bulanromawi = "V";
                    }
                    elseif ($bulan == "06") {
                      # code...
                      $bulanromawi = "VI";
                    }
                    elseif ($bulan == "07") {
                      # code...
                      $bulanromawi = "VII";
                    }
                    elseif ($bulan == "08") {
                      # code...
                      $bulanromawi = "VIII";
                    }
                    elseif ($bulan == "09") {
                      # code...
                      $bulanromawi = "IX";
                    }
                    elseif ($bulan == "X") {
                      # code...
                      $bulanromawi = "X";
                    }
                    elseif ($bulan == "11") {
                      # code...
                      $bulanromawi = "XI";
                    }
                    elseif ($bulan == "12") {
                      # code...
                      $bulanromawi = "XII";
                    }
                    $nofaktur = substr($invno,8,4)."/SPC/KW/".$bulanromawi."/".substr($periode, 0,4);

            $loginname = $this->ion_auth->user()->row()->username;  


            $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback


             );
            
        $this->invoice_model->create_invoice($data);

            /*
                Update Invoice Status
                */
        $inv_no = $key->Invoice_No;
        
        $datastatus = array(
            'Status' => 'CLOSED'
        );
     
        $wherestatus = array(
            'Invoice_No' => $inv_no
        );


        $this->invoice_model->update_proforma_status($wherestatus,$datastatus,'proforma');

        /* input proforma log */

        $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Dibuat'
         );
        $this->invoice_model->insert_proforma_log($datalog);
        
        }
        redirect('admin/invoice');

    }

    public function create_retail()
    {
            /*
            Insert To Invoice Table
            */
        $proformainv = $this->invoice_model->get_proforma_retail();
        $this->data['proforma_all'] = $proformainv;
        foreach ($proformainv as $key ) {
            # code...
            if($key->Sub_Product == "RETAIL"){
                $invtype = "EMAIL";
            }
            else{
                $invtype = "DELIVERY";
            }

            //nomor faktur
                    $invno = $key->Invoice_No;
                    $periode = $key->Periode;
                    $bulan = substr($periode, 5,2);
                    if ($bulan == "01") {
                      # code...
                      $bulanromawi = "I";
                    }
                    elseif ($bulan == "02") {
                      # code...
                      $bulanromawi = "II";
                    }
                    elseif ($bulan == "03") {
                      # code...
                      $bulanromawi = "III";
                    }
                    elseif ($bulan == "04") {
                      # code...
                      $bulanromawi = "IV";
                    }
                    elseif ($bulan == "05") {
                      # code...
                      $bulanromawi = "V";
                    }
                    elseif ($bulan == "06") {
                      # code...
                      $bulanromawi = "VI";
                    }
                    elseif ($bulan == "07") {
                      # code...
                      $bulanromawi = "VII";
                    }
                    elseif ($bulan == "08") {
                      # code...
                      $bulanromawi = "VIII";
                    }
                    elseif ($bulan == "09") {
                      # code...
                      $bulanromawi = "IX";
                    }
                    elseif ($bulan == "X") {
                      # code...
                      $bulanromawi = "X";
                    }
                    elseif ($bulan == "11") {
                      # code...
                      $bulanromawi = "XI";
                    }
                    elseif ($bulan == "12") {
                      # code...
                      $bulanromawi = "XII";
                    }
                    $nofaktur = substr($invno,8,4)."/SPC/KW/".$bulanromawi."/".substr($periode, 0,4);

            $loginname = $this->ion_auth->user()->row()->username;  


            $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback


             );
            
        $this->invoice_model->create_invoice($data);

            /*
                Update Invoice Status
                */
        $inv_no = $key->Invoice_No;
        
        $datastatus = array(
            'Status' => 'CLOSED'
        );
     
        $wherestatus = array(
            'Invoice_No' => $inv_no
        );


        $this->invoice_model->update_proforma_status($wherestatus,$datastatus,'proforma');

        /* input proforma log */

        $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Dibuat'
         );
        $this->invoice_model->insert_proforma_log($datalog);
        
        }
        redirect('admin/invoice/retail');

    }

            public function create_one()
    {
            /*
            Insert To Invoice Table
            */
        $no_inv = $this->uri->segment(4);
        $proformainv = $this->invoice_model->get_proforma_by_no($no_inv);
        $this->data['proforma_all'] = $proformainv;
        foreach ($proformainv as $key ) {
            # code...
            if($key->Sub_Product == "RETAIL"){
                $invtype = "EMAIL";
            }
            else{
                $invtype = "DELIVERY";
            }

            //nomor faktur
                    $invno = $key->Invoice_No;
                    $periode = $key->Periode;
                    $bulan = substr($periode, 5,2);
                    if ($bulan == "01") {
                      # code...
                      $bulanromawi = "I";
                    }
                    elseif ($bulan == "02") {
                      # code...
                      $bulanromawi = "II";
                    }
                    elseif ($bulan == "03") {
                      # code...
                      $bulanromawi = "III";
                    }
                    elseif ($bulan == "04") {
                      # code...
                      $bulanromawi = "IV";
                    }
                    elseif ($bulan == "05") {
                      # code...
                      $bulanromawi = "V";
                    }
                    elseif ($bulan == "06") {
                      # code...
                      $bulanromawi = "VI";
                    }
                    elseif ($bulan == "07") {
                      # code...
                      $bulanromawi = "VII";
                    }
                    elseif ($bulan == "08") {
                      # code...
                      $bulanromawi = "VIII";
                    }
                    elseif ($bulan == "09") {
                      # code...
                      $bulanromawi = "IX";
                    }
                    elseif ($bulan == "X") {
                      # code...
                      $bulanromawi = "X";
                    }
                    elseif ($bulan == "11") {
                      # code...
                      $bulanromawi = "XI";
                    }
                    elseif ($bulan == "12") {
                      # code...
                      $bulanromawi = "XII";
                    }
                    $nofaktur = substr($invno,8,4)."/SPC/KW/".$bulanromawi."/".substr($periode, 0,4);

            $loginname = $this->ion_auth->user()->row()->username;  


            $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback



             );
            
        $this->invoice_model->create_invoice($data);

            /*
                Update Invoice Status
                */
        $inv_no = $key->Invoice_No;
        
        $datastatus = array(
            'Status' => 'CLOSED'
        );
     
        $wherestatus = array(
            'Invoice_No' => $inv_no
        );


        $this->invoice_model->update_proforma_status($wherestatus,$datastatus,'proforma');

        /* input proforma log */

        $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Dibuat'
         );
        $this->invoice_model->insert_proforma_log($datalog);
        
        }
        redirect('admin/invoice/proforma');

    }

        public function create_retail_one()
    {
            /*
            Insert To Invoice Table
            */
        $no_inv = $this->uri->segment(4);
        $proformainv = $this->invoice_model->get_proforma_by_no($no_inv);
        $this->data['proforma_all'] = $proformainv;
        foreach ($proformainv as $key ) {
            # code...
            if($key->Sub_Product == "RETAIL"){
                $invtype = "EMAIL";
            }
            else{
                $invtype = "DELIVERY";
            }

            //nomor faktur
                    $invno = $key->Invoice_No;
                    $periode = $key->Periode;
                    $bulan = substr($periode, 5,2);
                    $tahun = substr($periode, 0,4);
                    if ($bulan == "01") {
                      # code...
                      $bulanromawi = "I";
                    }
                    elseif ($bulan == "02") {
                      # code...
                      $bulanromawi = "II";
                    }
                    elseif ($bulan == "03") {
                      # code...
                      $bulanromawi = "III";
                    }
                    elseif ($bulan == "04") {
                      # code...
                      $bulanromawi = "IV";
                    }
                    elseif ($bulan == "05") {
                      # code...
                      $bulanromawi = "V";
                    }
                    elseif ($bulan == "06") {
                      # code...
                      $bulanromawi = "VI";
                    }
                    elseif ($bulan == "07") {
                      # code...
                      $bulanromawi = "VII";
                    }
                    elseif ($bulan == "08") {
                      # code...
                      $bulanromawi = "VIII";
                    }
                    elseif ($bulan == "09") {
                      # code...
                      $bulanromawi = "IX";
                    }
                    elseif ($bulan == "X") {
                      # code...
                      $bulanromawi = "X";
                    }
                    elseif ($bulan == "11") {
                      # code...
                      $bulanromawi = "XI";
                    }
                    elseif ($bulan == "12") {
                      # code...
                      $bulanromawi = "XII";
                    }
                    $nofaktur = substr($invno,8,4)."/SPC/KW/".$bulanromawi."/".substr($periode, 0,4);

            $loginname = $this->ion_auth->user()->row()->username;  


            $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback


             );
            
        $this->invoice_model->create_invoice($data);

            /*
                Update Invoice Status
                */
        $inv_no = $key->Invoice_No;
        
        $datastatus = array(
            'Status' => 'CLOSED'
        );
     
        $wherestatus = array(
            'Invoice_No' => $inv_no
        );


        $this->invoice_model->update_proforma_status($wherestatus,$datastatus,'proforma');

        /* input proforma log */

        $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Dibuat'
         );
        $this->invoice_model->insert_proforma_log($datalog);
        $redirect1 = "http://172.31.12.154/mail/createinv.php?tahun=".$tahun."&bulan=".$bulan."&inv_no=".$inv_no;
        
        }
        redirect($redirect1);

    }
public function create_retail_one_bekasi()
    {
            /*
            Insert To Invoice Table
            */
        $no_inv = $this->uri->segment(4);
        $proformainv = $this->invoice_model->get_proforma_by_no($no_inv);
        $this->data['proforma_all'] = $proformainv;
        foreach ($proformainv as $key ) {
            # code...
            if($key->Sub_Product == "RETAIL"){
                $invtype = "EMAIL";
            }
            else{
                $invtype = "DELIVERY";
            }

            //nomor faktur
                    $invno = $key->Invoice_No;
                    $periode = $key->Periode;
                    $bulan = substr($periode, 5,2);
                    $tahun = substr($periode, 0,4);
                    if ($bulan == "01") {
                      # code...
                      $bulanromawi = "I";
                    }
                    elseif ($bulan == "02") {
                      # code...
                      $bulanromawi = "II";
                    }
                    elseif ($bulan == "03") {
                      # code...
                      $bulanromawi = "III";
                    }
                    elseif ($bulan == "04") {
                      # code...
                      $bulanromawi = "IV";
                    }
                    elseif ($bulan == "05") {
                      # code...
                      $bulanromawi = "V";
                    }
                    elseif ($bulan == "06") {
                      # code...
                      $bulanromawi = "VI";
                    }
                    elseif ($bulan == "07") {
                      # code...
                      $bulanromawi = "VII";
                    }
                    elseif ($bulan == "08") {
                      # code...
                      $bulanromawi = "VIII";
                    }
                    elseif ($bulan == "09") {
                      # code...
                      $bulanromawi = "IX";
                    }
                    elseif ($bulan == "X") {
                      # code...
                      $bulanromawi = "X";
                    }
                    elseif ($bulan == "11") {
                      # code...
                      $bulanromawi = "XI";
                    }
                    elseif ($bulan == "12") {
                      # code...
                      $bulanromawi = "XII";
                    }
                    $nofaktur = substr($invno,8,4)."/SPC/KW/".$bulanromawi."/".substr($periode, 0,4);

            $loginname = $this->ion_auth->user()->row()->username;  


            $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback


             );
            
        $this->invoice_model->create_invoice($data);

            /*
                Update Invoice Status
                */
        $inv_no = $key->Invoice_No;
        
        $datastatus = array(
            'Status' => 'CLOSED'
        );
     
        $wherestatus = array(
            'Invoice_No' => $inv_no
        );


        $this->invoice_model->update_proforma_status($wherestatus,$datastatus,'proforma');

        /* input proforma log */

        $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Dibuat'
         );
        $this->invoice_model->insert_proforma_log($datalog);
        
        $redirect1 = "http://172.31.12.154/mail/createinvbekasi.php?tahun=".$tahun."&bulan=".$bulan."&inv_no=".$inv_no;
        
        }
        redirect($redirect1);

    }

public function create_retail_instalasi_bekasi()
    {
            /*
            Insert To Invoice Table
            */
        $no_inv = $this->uri->segment(4);
        $proformainv = $this->invoice_model->get_proforma_by_no($no_inv);
        $this->data['proforma_all'] = $proformainv;
        foreach ($proformainv as $key ) {
            # code...
            if($key->Sub_Product == "RETAIL"){
                $invtype = "EMAIL";
            }
            else{
                $invtype = "DELIVERY";
            }

            //nomor faktur
                    $invno = $key->Invoice_No;
                    $periode = $key->Periode;
                    $bulan = substr($periode, 5,2);
                    $tahun = substr($periode, 0,4);
                    if ($bulan == "01") {
                      # code...
                      $bulanromawi = "I";
                    }
                    elseif ($bulan == "02") {
                      # code...
                      $bulanromawi = "II";
                    }
                    elseif ($bulan == "03") {
                      # code...
                      $bulanromawi = "III";
                    }
                    elseif ($bulan == "04") {
                      # code...
                      $bulanromawi = "IV";
                    }
                    elseif ($bulan == "05") {
                      # code...
                      $bulanromawi = "V";
                    }
                    elseif ($bulan == "06") {
                      # code...
                      $bulanromawi = "VI";
                    }
                    elseif ($bulan == "07") {
                      # code...
                      $bulanromawi = "VII";
                    }
                    elseif ($bulan == "08") {
                      # code...
                      $bulanromawi = "VIII";
                    }
                    elseif ($bulan == "09") {
                      # code...
                      $bulanromawi = "IX";
                    }
                    elseif ($bulan == "X") {
                      # code...
                      $bulanromawi = "X";
                    }
                    elseif ($bulan == "11") {
                      # code...
                      $bulanromawi = "XI";
                    }
                    elseif ($bulan == "12") {
                      # code...
                      $bulanromawi = "XII";
                    }
                    $nofaktur = substr($invno,8,4)."/SPC/KW/".$bulanromawi."/".substr($periode, 0,4);

            $loginname = $this->ion_auth->user()->row()->username;  


            $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback


             );
            
        $this->invoice_model->create_invoice($data);

            /*
                Update Invoice Status
                */
        $inv_no = $key->Invoice_No;
        
        $datastatus = array(
            'Status' => 'CLOSED'
        );
     
        $wherestatus = array(
            'Invoice_No' => $inv_no
        );


        $this->invoice_model->update_proforma_status($wherestatus,$datastatus,'proforma');

        /* input proforma log */

        $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Dibuat'
         );
        $this->invoice_model->insert_proforma_log($datalog);
        $redirect1 = "http://172.31.12.154/mail/createinvinstalasibekasi.php?tahun=".$tahun."&bulan=".$bulan."&inv_no=".$inv_no;
        
        }
        redirect($redirect1);

    }

    public function create_retail_instalasi()
    {
            /*
            Insert To Invoice Table
            */
        $no_inv = $this->uri->segment(4);
        $proformainv = $this->invoice_model->get_proforma_by_no($no_inv);
        $this->data['proforma_all'] = $proformainv;
        foreach ($proformainv as $key ) {
            # code...
            if($key->Sub_Product == "RETAIL"){
                $invtype = "EMAIL";
            }
            else{
                $invtype = "DELIVERY";
            }

            //nomor faktur
                    $invno = $key->Invoice_No;
                    $periode = $key->Periode;
                    $bulan = substr($periode, 5,2);
                    $tahun = substr($periode, 0,4);
                    if ($bulan == "01") {
                      # code...
                      $bulanromawi = "I";
                    }
                    elseif ($bulan == "02") {
                      # code...
                      $bulanromawi = "II";
                    }
                    elseif ($bulan == "03") {
                      # code...
                      $bulanromawi = "III";
                    }
                    elseif ($bulan == "04") {
                      # code...
                      $bulanromawi = "IV";
                    }
                    elseif ($bulan == "05") {
                      # code...
                      $bulanromawi = "V";
                    }
                    elseif ($bulan == "06") {
                      # code...
                      $bulanromawi = "VI";
                    }
                    elseif ($bulan == "07") {
                      # code...
                      $bulanromawi = "VII";
                    }
                    elseif ($bulan == "08") {
                      # code...
                      $bulanromawi = "VIII";
                    }
                    elseif ($bulan == "09") {
                      # code...
                      $bulanromawi = "IX";
                    }
                    elseif ($bulan == "X") {
                      # code...
                      $bulanromawi = "X";
                    }
                    elseif ($bulan == "11") {
                      # code...
                      $bulanromawi = "XI";
                    }
                    elseif ($bulan == "12") {
                      # code...
                      $bulanromawi = "XII";
                    }
                    $nofaktur = substr($invno,8,4)."/SPC/KW/".$bulanromawi."/".substr($periode, 0,4);

            $loginname = $this->ion_auth->user()->row()->username;  


            $data = array(  'Location' => 'SPC-HO',
                            'Inv_no' => $key->Invoice_No,
                            'Invoice_Type' => $invtype,
                            'Account_No' => $key->Account_No,
                            'Inv_Date' => $key->Periode,
                            'Account_Name' => $key->Account_Name,
                            'Account_Sub_Name' => $key->Account_Sub_Name,
                            'Account_Sub_Address' => $key->Account_Sub_Address,
                            'Faktur_No' => $nofaktur,
                            'Faktur_Date' => $key->Periode,
                            'Subtotal' => $key->Subtotal,
                            'PPN' => $key->PPN,
                            'Discount_Val' => $key->Diskon,
                            'Other_Val' => $key->Restitusi,
                            'Status' => 'CREATED',
                            'Created_By' => $loginname,
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Total' => $key->Total,
                            'City' => $key->City,
                            'Note' => $key->Note,
                            'Invoice_Category' => $key->Invoice_Category,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback


             );
            
        $this->invoice_model->create_invoice($data);

            /*
                Update Invoice Status
                */
        $inv_no = $key->Invoice_No;
        
        $datastatus = array(
            'Status' => 'CLOSED'
        );
     
        $wherestatus = array(
            'Invoice_No' => $inv_no
        );


        $this->invoice_model->update_proforma_status($wherestatus,$datastatus,'proforma');

        /* input proforma log */

        $datalog = array(   'Inv_No' => $inv_no,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Dibuat'
         );
        $this->invoice_model->insert_proforma_log($datalog);
        $redirect1 = "http://172.31.12.154/mail/createinvinstalasi.php?tahun=".$tahun."&bulan=".$bulan."&inv_no=".$inv_no;
        
        }
        redirect($redirect1);

    }
    



     public function preview_invoice()
    {
            /* Title Page */

            /*Proforma*/

            
            $inv_no = $this->uri->segment(4);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_preview_invoice', $this->data);
    }

    public function print_invoice($no_inv)
    {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            
            $inv_no = $this->uri->segment(4);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_preview_invoice_old', $this->data);


            /* inser invoice log*/  
            $loginname = $this->ion_auth->user()->row()->username;   
            $datalog = array(   'Inv_No' => $datainv[0]->Inv_No,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Di Print'
            );
            $this->invoice_model->insert_Invoice_log($datalog);
    }

    public function print_invoice_dedicated($no_inv)
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            
            $inv_no = $this->uri->segment(4);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_print_invoice_dedicated', $this->data);


            /* inser invoice log*/  
            $loginname = $this->ion_auth->user()->row()->username;   
            $datalog = array(   'Inv_No' => $datainv[0]->Inv_No,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Di Print'
            );
            $this->invoice_model->insert_Invoice_log($datalog);

            
        }
    }
    public function print_kwitansi()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

       

            $inv_no = $this->uri->segment(4);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_print_kwitansi', $this->data);


            /* inser invoice log*/  
            $loginname = $this->ion_auth->user()->row()->username;   
            $datalog = array(   'Inv_No' => $datainv[0]->Inv_No,
                            'Created_By' => $loginname,
                            'Status' => 'Kwitansi Di Print'
            );
            $this->invoice_model->insert_Invoice_log($datalog);


            
        }
    }

    public function summary(){

        $this->template->admin_render('admin/invoice/v_summary', $this->data);
    }

    public function proforma_log(){

        $this->template->admin_render('admin/invoice/v_proforma_log', $this->data);
    }

    public function invoice_log(){

        $this->template->admin_render('admin/invoice/v_invoice_log', $this->data);
    }

public function print_invoice_instalasi($no_inv)
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/

            
            $inv_no = $this->uri->segment(4);
            $datainv = $this->invoice_model->get_invoice_no($inv_no);
            $this->data['inv'] = $datainv;
            /* Load Template */
            //$this->template->admin_render('admin/invoice/v_print_invoice', $this->data);
            $this->load->view('admin/invoice/v_print_invoice_installasi', $this->data);


            /* inser invoice log*/  
            $loginname = $this->ion_auth->user()->row()->username;   
            $datalog = array(   'Inv_No' => $datainv[0]->Inv_No,
                            'Created_By' => $loginname,
                            'Status' => 'Invoice Di Print'
            );
            $this->invoice_model->insert_Invoice_log($datalog);

            
        }
    }

    public function bulk(){

        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            //$invoicearray = $this->invoice_model->get_name_bulk();
            //return $this->output
                //->set_content_type('application/json')
                //->set_status_header(500)
                //->set_output(json_encode($invoicearray));

            //$city = $this->input->get('city' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
                $this->data['invoice_all'] = $this->invoice_model->get_name_bulk();

            

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice_bulk', $this->data);

            
        }
        
    }

    public function preview_bulk(){
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $name = $this->input->get('name' , TRUE);
            $product = $this->input->get('product' , TRUE);
            $bulan = $this->input->get('bulan' , TRUE);
            $tahun = $this->input->get('tahun' , TRUE);
            $this->data['inv'] = $this->invoice_model->get_detail_bulk($name,$product,$bulan,$tahun);
            $this->load->view('admin/invoice/v_print_bulk', $this->data);
            //return $this->output
                //->set_content_type('application/json')
                //->set_status_header(500)
                //->set_output(json_encode($invoicearray));

            
        }
    }

    public function test()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $city = $this->input->get('city' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            if(is_null($city)){
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated();
            }
            else{
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated_city($city);
            }

            

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice_auto', $this->data);

            
        }
    }

    public function create_proforma_instalasi()
    {
            /*
            Insert To Invoice Table
            */
        $regid = $this->uri->segment(4);
        $data = $this->master_model->get_customer_regid($regid);
        $key = $data[0];
        $taskid = $key->Task_ID;
        $year = date("y");
        $month= date("m");
        $number = sprintf("%08d", $taskid);
        $inv = "INS".$year."".$month."".$number;
        $prf = "PRF-".$inv;
        $year4 = date("Y");
        $periode = $year4."-".$month."-01";
        $total = (int)$key->Installation*1.1;
        $ppn = (int)$key->Installation*0.1;



            $data = array( 
                            'Location' => $key->City,
                            'Proforma_No' => $prf,
                            'Invoice_No' => $inv,
                            'Account_No' => $key->Customer_ID,
                            'Account_Name' => $key->Customer_Name,
                            'Account_Sub_Name' => $key->Customer_Sub_Name,
                            'Account_Sub_Address' => $key->Customer_Sub_Address,
                            'City' => $key->City,
                            'Proforma_Type' => $key->Billing_Type,
                            'Subtotal' => $key->Installation,
                            'Total' => $total,
                            'PPN' => $ppn,
                            'PPH' => '0',
                            'Periode' => $periode,
                            'Status' => 'CREATED',
                            'Invoice_Category' => 'installation',
                            'Restitusi' => '0',
                            'Diskon' => '0',
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback

             );
            
        $this->invoice_model->create_proforma($data);

        redirect('admin/customer/detail/'.$key->Reg_ID);

    }

    public function create_proforma_prorate()
    {
            /*
            Insert To Invoice Table
            */
            function tgl_indo_1($tanggal){
              $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
                
              );
              $pecahkan = explode('-', $tanggal);
              
              // variabel pecahkan 0 = tanggal
              // variabel pecahkan 1 = bulan
              // variabel pecahkan 2 = tahun
             
              return $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
            }
        $regid = $this->input->post('regid');
        $akhir = $this->input->post('akhir');
        $awal = $this->input->post('awal');
        $selisih = ((int)$akhir-(int)$awal+1)/(int)$akhir;
        $data = $this->master_model->get_customer_regid($regid);
        $key = $data[0];
        $taskid = $key->Task_ID;
        $year = date("y");
        $month= date("m");
        $number = sprintf("%08d", $taskid);
        $inv = "PRT".$year."".$month."".$number;
        $prf = "PRF-".$inv;
        $year4 = date("Y");
        $periode = date("Y-m-01",strtotime($key->Start_Billing));
        if($key->Monthly_PPN == "INCLUDE"){
            $subtotal = (int)$key->Monthly_Price*100/110*$selisih;
        }
        else {
            # code...
            $subtotal = (int)$key->Monthly_Price*$selisih;
        }
        $start = $key->Start_Billing;

        $total = (int)$subtotal*1.1;
        $ppn = (int)$subtotal*0.1;
        $note = "Periode ".$awal."-".$akhir." ". tgl_indo_1($start);



            $data = array( 
                            'Location' => $key->City,
                            'Proforma_No' => $prf,
                            'Invoice_No' => $inv,
                            'Account_No' => $key->Customer_ID,
                            'Account_Name' => $key->Customer_Name,
                            'Account_Sub_Name' => $key->Customer_Sub_Name,
                            'Account_Sub_Address' => $key->Customer_Sub_Address,
                            'City' => $key->City,
                            'Proforma_Type' => $key->Billing_Type,
                            'Subtotal' => $subtotal,
                            'Total' => round($total,0),
                            'PPN' => $ppn,
                            'PPH' => '0',
                            'Periode' => $periode,
                            'Status' => 'CREATED',
                            'Invoice_Category' => 'monthly',
                            'Restitusi' => '0',
                            'Diskon' => '0',
                            'Sub_Product' => $key->Sub_Product,
                            'Bandwidth' => $key->Bandwidth,
                            'Email' => $key->Email,
                            'Reg_ID' => $key->Reg_ID,
                            'Handphone' => $key->Handphone,
                            'Phone' => $key->Phone,
                            'Start_Billing' => $key->Start_Billing,
                            'Region' => $key->Region,
                            'Note' => $note,
                            'id_mitra'=> $key->id_mitra,
                            'cashback'=> $key->cashback

             );
            
        $this->invoice_model->create_proforma($data);

        redirect('admin/customer/detail/'.$key->Reg_ID);

    }

    public function wa()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            // $city = $this->input->get('city' , TRUE);
            // /* Title Page */
            // $this->page_title->push(lang('menu_dashboard'));
            // $this->data['pagetitle'] = $this->page_title->show();

            // /* Breadcrumbs */
            // $this->data['breadcrumb'] = $this->breadcrumbs->show();

            // /*Proforma*/
            // if(is_null($city)){
            //     $this->data['invoice_all'] = $this->invoice_model->get_invoice_retail();
            // }
            // else{
            //     $this->data['invoice_all'] = $this->invoice_model->get_invoice_retail_city($city);
            // }

            $this->data['whatsapp']=$this->invoice_model->get_whatsapp();

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_wa', $this->data);

            
        }
    }
    public function Manual()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {   

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice_manual', $this->data);

            
        }
    }
     public function Progres_bulk()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {   
             $city = $this->input->get('city' , TRUE);
            $invdate = $this->input->get('invdate' , TRUE);
            $name = $this->input->get('name' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            //if(is_null($city)){
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated_bulk($city,$invdate,$name);
            /* Load Template */
            $this->template->admin_render('admin/invoice/v_progres_bulk', $this->data);

            
        }
    }
    public function Progres_bulk_retail()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {   
             $city = $this->input->get('city' , TRUE);
            $invdate = $this->input->get('invdate' , TRUE);
            $name = $this->input->get('name' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /*Proforma*/
            //if(is_null($city)){
                $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated_bulk($city,$invdate,$name);
            /* Load Template */
            $this->template->admin_render('admin/invoice/v_progres_bulk_retail', $this->data);

            
        }
    }

    public function user()
    {
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $regid = $this->uri->segment(4);
            $city = $this->input->get('city' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            $this->data['invoice_all'] = $this->invoice_model->get_invoice_per_user($regid);
            

            /* Load Template */
            $this->template->admin_render('admin/invoice/v_invoice_per_user', $this->data);

            
        }
    }


    public function upload(){

        $no_inv = $this->input->post('no_inv' , TRUE);
        $reg_id = $this->input->post('reg_id' , TRUE);
        $metode = $this->input->post('metode' , TRUE);
        $bank = $this->input->post('bank' , TRUE);
        $totalinvoice = $this->input->post('totalinvoice' , TRUE);
        $totalbayar = $this->input->post('totalbayar' , TRUE);

        $config['upload_path']          = '/Applications/XAMPP/htdocs/biling/upload/buktibayar/';
        $config['allowed_types']        = 'png|jpg';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        $config['file_name']            = $no_inv;
 
        $this->load->library('upload', $config);
 
        if ( ! $this->upload->do_upload('berkas')){
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('v_upload', $error);
        }else{
            //data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name'];
            //$this->load->view('v_upload_sukses', $data);

            $databayar = array(
            'No_Inv' => $no_inv,
            'Reg_ID' => $reg_id,
            'Metode' => $metode,
            'Bank' => $bank,
            'Nilai_Invoice' => $totalinvoice,
            'Total_Bayar' => $totalbayar,
            'Bukti_Bayar' => $file_name
        );

        $this->invoice_model->insert_log_pembayaran($databayar);
        }

        
    }

    public function preview_custom(){
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $name = $this->input->get('name' , TRUE);
            $product = $this->input->get('product' , TRUE);
            $bulan = $this->input->get('bulan' , TRUE);
            $tahun = $this->input->get('tahun' , TRUE);
            $this->data['inv'] = $this->invoice_model->get_detail_custom($name);
            $this->load->view('admin/invoice/v_print_custom', $this->data);
            //return $this->output
                //->set_content_type('application/json')
                //->set_status_header(500)
                //->set_output(json_encode($invoicearray));

            
        }
    }

    public function preview_per_user(){
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $arrayinvoice = $this->input->get('arrayinvoice' , TRUE);
            $this->data['inv'] = $this->invoice_model->get_detail_per_user($arrayinvoice);
            $this->load->view('admin/invoice/v_print_custom', $this->data);
            //return $this->output
                //->set_content_type('application/json')
                //->set_status_header(500)
                //->set_output(json_encode($invoicearray));

            
        }
    }

    public function aksi_upload(){
        $config['upload_path']          = 'assets/upload/bukti_bayar';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100;
        //$config['max_width']            = 1024;
        //$config['max_height']           = 768;
 
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
 
        if ( ! $this->upload->do_upload('berkas')){
            $error = array('error' => $this->upload->display_errors());
            //$this->load->view('v_upload', $error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            //$this->load->view('v_upload_sukses', $data);
        }
    }

    public function tesupload(){
    $this->load->view('admin/invoice/v_upload', array('error' => ' ' ));
    }

     public function invoice_close(){
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
             $city = $this->input->get('city' , TRUE);
            $invdate = $this->input->get('invdate' , TRUE);
            $name = $this->input->get('name' , TRUE);
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

         $this->data['invoice_all'] = $this->invoice_model->get_invoice_dedicated_closed($city,$invdate,$name);
         $this->data['invoice_all'] = $this->invoice_model->get_invoice_closed_retail($city,$invdate,$name);

        $this->template->admin_render('admin/invoice/v_invoice_close', $this->data);
        }
    }
    
}
