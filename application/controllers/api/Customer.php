<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends Public_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/invoice_model');
        $this->load->model('admin/master_model');
    }

    public function regid(){
        $urut = $this->input->get('urut' , TRUE);
        $invoicearray = $this->master_model->get_regid($urut);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function name(){
        $invoicearray = $this->master_model->get_account_name();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    
}
