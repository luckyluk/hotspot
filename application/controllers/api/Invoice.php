<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends Public_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/invoice_model');
        $this->load->model('admin/master_model');
    }


    public function where()
    {
        $invdate = $this->input->get('inv_date' , TRUE);
        $subproduct = $this->input->get('sub_product', TRUE);
        $name = $this->input->get('name' , TRUE);
        $subname = $this->input->get('subname' , TRUE);

            $arrs = array();
     
            if($invdate != null){
                $arrs[] = array('Inv_Date' => $invdate);
            }

            if($subproduct != null){
                $arrs[] = array('Sub_Product' => $subproduct);
            } 

            if($name != null){
                $arrs[] = array('Account_Name' => $name);
            } 

            if($subname != null){
                $arrs[] = array('Account_Sub_Name' => $subname);
            } 


            $arrs[] = array('Status' => 'CLOSED');


            $list = array();
            foreach ($arrs as $arr) {
                # code...
                if(is_array($arr)){
                    $list = array_merge($list, $arr);
                }
            }
            $where = $list;

        $invoicearray = $this->data['invoice'] = $this->invoice_model->get_invoice_filter($where);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }


    public function name(){

        $name = $this->invoice_model->get_account_name();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($name));

    }

    public function subname($name){
        $name1 = $this->uri->segment(4);
        $name = str_replace("%20"," ",$name);
        $where = array(
            'Account_Name' => $name
        );


        $name = $this->invoice_model->get_account_sub_name($where);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($name));

    }

    public function subproduct(){

        $name = $this->invoice_model->get_sub_product();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($name));

    }

    public function invdate(){

        $name = $this->invoice_model->get_invoice_date();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($name));

    }

    public function city(){

        $name = $this->invoice_model->get_city();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($name));

    }

    public function proforma_log(){
        $invno = $this->input->get('invno' , TRUE);

            $arrs = array();
     
            if($invno != null){
                $arrs[] = array('Inv_No' => $invno);
            }

            $list = array();
            foreach ($arrs as $arr) {
                # code...
                if(is_array($arr)){
                    $list = array_merge($list, $arr);
                }
            }
            $where = $list;

        $invoicearray = $this->invoice_model->get_proforma_log($where);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function invoice_log(){
        $invno = $this->input->get('invno' , TRUE);

            $arrs = array();
     
            if($invno != null){
                $arrs[] = array('Inv_No' => $invno);
            }

            $list = array();
            foreach ($arrs as $arr) {
                # code...
                if(is_array($arr)){
                    $list = array_merge($list, $arr);
                }
            }
            $where = $list;

        $invoicearray = $this->invoice_model->get_invoice_log($where);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function invoice_month(){
        $tahun = $this->input->get('tahun' , TRUE);
        $bulan = $this->input->get('bulan', TRUE);
        $date = $tahun."-".$bulan."-01";

        $invoicearray = $this->invoice_model->get_invoice_created_by_month($date);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function invoice_closed_month(){
        $tahun = $this->input->get('tahun' , TRUE);
        $bulan = $this->input->get('bulan', TRUE);
        $date = $tahun."-".$bulan."-01";

        $invoicearray = $this->invoice_model->get_invoice_closed_by_month($date);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function value(){

        $invoicearray = $this->invoice_model->get_value_invoice_created();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }


public function value_closed(){

        $invoicearray = $this->invoice_model->get_value_invoice_closed();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }
    public function value_month(){
        $tahun = $this->input->get('tahun' , TRUE);
        $bulan = $this->input->get('bulan', TRUE);
        $date = $tahun."-".$bulan."-01";

        $invoicearray = $this->invoice_model->get_value_invoice_created_by_month($date);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function value_closed_month(){
        $tahun = $this->input->get('tahun' , TRUE);
        $bulan = $this->input->get('bulan', TRUE);
        $date = $tahun."-".$bulan."-01";

        $invoicearray = $this->invoice_model->get_value_invoice_closed_by_month($date);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }
    public function test(){

        
        echo $this->db->last_query($this->invoice_model->get_join());
        //return $this->output
          //  ->set_content_type('application/json')
            //->set_status_header(500)
            //->set_output(json_encode($invoicearray));
            //$this->output->enable_profiler(TRUE);
    }

    public function filter()
    {
        $invdate = $this->input->get('inv_date' , TRUE);
        $subproduct = $this->input->get('sub_product', TRUE);
        $name = $this->input->get('name' , TRUE);
        $subname = $this->input->get('subname' , TRUE);

            $arrs = array();
     
            if($invdate != null){
                $arrs[] = array('Inv_Date' => $invdate);
            }

            if($subproduct != null){
                $arrs[] = array('Sub_Product' => $subproduct);
            } 

            if($name != null){
                $arrs[] = array('Account_Name' => $name);
            } 

            if($subname != null){
                $arrs[] = array('Account_Sub_Name' => $subname);
            } 


            $arrs[] = array('Status' => 'CLOSED');


            $list = array();
            foreach ($arrs as $arr) {
                # code...
                if(is_array($arr)){
                    $list = array_merge($list, $arr);
                }
            }
            $where = $list;

        $invoicearray = $this->data['invoice'] = $this->invoice_model->get_invoice_filter($where);
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    
}
