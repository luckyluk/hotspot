<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daerah extends Public_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/daerah_model');
    }

    public function provinsi(){
        $urut = $this->input->get('urut' , TRUE);
        $invoicearray = $this->daerah_model->get_province_all();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }

    public function kota(){
        $invoicearray = $this->daerah_model->get_city();
        return $this->output
            ->set_content_type('application/json')
            //->set_status_header(500)
            ->set_output(json_encode($invoicearray));
    }


    
}
